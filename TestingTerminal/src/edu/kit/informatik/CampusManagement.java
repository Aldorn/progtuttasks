package edu.kit.informatik;

/**
 * This is just an example class. Imagine this as the userinterface of a more complex program.
 * @author moritz
 *
 */
public class CampusManagement {
        
    
    public static void main(String[] args) {
        boolean quit = false;
        do {
            switch(Terminal.readLine()) {
            case "add student":
                Terminal.printLine("OK");
                break;
            case "list students": 
                Terminal.printLine("Paul");
                Terminal.printLine("Emil");
                break;
            case "get nrofstudents": 
                Terminal.printLine("Number of Students: 2");
            case "quit":
                quit = true;
                break;
            default:
                Terminal.printLine("Error: unknown command");
            }
        } while (!quit);
    }
}
