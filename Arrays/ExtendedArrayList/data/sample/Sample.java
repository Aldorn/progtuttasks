import org.junit.Test;

import test.TestObject;
import test.TestedClass;
import test.TestedProgram;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static test.Clazz.cs;

public class Sample {
	private final TestedClass TutorialArrayList = TestedProgram.getClass("edu.kit.informatik.util.TutorialArrayList");

	@Test
	public void sample() {
		final TestObject islandBooks = TutorialArrayList.nev(2);
		islandBooks.runVoid("add", cs(Object.class), "Harry Potter");
		islandBooks.runVoid("add", cs(Object.class), "The Catcher in the Rye");
		islandBooks.runVoid("add", cs(Object.class), "The Art of Computer Programming");
		islandBooks.runVoid("insertAt", cs(Object.class, int.class), "Lord of the Rings", 1);
		islandBooks.runVoid("insertAt", cs(Object.class, int.class), "Lord of the Rings", 1);
		assertThat(islandBooks.run(int.class, "getSize"), is(5));
		assertThat(islandBooks.run(int.class, "getReservedSize"), is(8));
		islandBooks.runVoid("removeAt", 3);
		assertThat(islandBooks.run(boolean.class, "remove", cs(Object.class), "Lord of the Rings"), is(true));
		assertThat(islandBooks.run(int.class, "getReservedSize"), is(4));
		assertThat(islandBooks.run(boolean.class, "remove", cs(Object.class), "The Catcher in the Rye"), is(false));
	}
}
