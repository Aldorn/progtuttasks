import org.junit.Test;

import test.Input;

import static test.TestedProgram.main;
import static test.matchers.CLIMatchers.*;
import static org.junit.Assert.assertThat;

public class T50ExampleTest {

	@Test
	public void correctExample() {
		String mazeDef = Input.getFile(
			//@formatter:off
			"┌─┬────────┐",
			"│╷│┌──────╴│",
			"││││ ┌──┬╴╶┤",
			"││││ │╷╶┼╴╷│",
			"││╵│ └┘ │ ││",
			"│└─┴────┴╴╵│",
			"╵╶─────────┘"
			//@formatter:on
		);

		String routeDef = Input.getFile(
			//@formatter:off
			"(0,0)→↓↓↓↓→↑↑↑↑→→→→→→→→↓←↓→↓↓↓",
			"(10,5)↑↑→→→↓←↓",
			"(-2,8)→↑→↑↑→→→→→→→→→↑←",
			"(2,5)",
			"(1,5)→→→←"
			//@formatter:on
		);

		String[] output = {
			//@formatter:off
			"(10,5)",
			"lucky",
			"(8,4)",
			"(2,5)",
			"(0,5)"
			//@formatter:on};
		};

		assertThat(main(mazeDef, routeDef), prints(output));
	}

	@Test
	public void badWallDefinitionExample() {
		String mazeDef = Input.getFile(
			//@formatter:off
			"┌┐",
			"└"
			//@formatter:on
		);

		String routeDef = Input.getFile(
			//@formatter:off
			"(0,0)→↓"
			//@formatter:on
		);

		assertThat(main(mazeDef, routeDef), printsErrorStartingWith(
				"syntax error in maze definition, line 2, character 2: the wall definition does not fit to its upper one"));
	}

	@Test
	public void badCharacterInRoute() {
		String mazeDef = Input.getFile(
				//@formatter:off
				"┌┐",
				"└┘"
				//@formatter:on
			);

			String routeDef = Input.getFile(
				//@formatter:off
				"(0,0)→↓",
				"(10,5)↑↑→→→↓←↓",
				"(1,5)→f→←"
				//@formatter:on
			);
			
			String[] output = {
				//@formatter:off
				"(0,0)",
				"(12,5)"
				//@formatter:on
			};

			assertThat(main(mazeDef, routeDef), printsAndErrorStartingWith(output, 
					"syntax error in route definition, line 3, character 7: expected a direction character, but found 'f'"));
	}
}
