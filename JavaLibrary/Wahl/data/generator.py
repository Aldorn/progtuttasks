#!/usr/bin/python3

import math
from random import randint as r, choice
from argparse import ArgumentParser as Parser
from pathlib import Path

chars = ''.join(chr(c) for c in range(ord('a'), ord('z')))

def gen_names(n = 4, min_len = 5, max_len = 10):
    names = set()
    while(len(names) < n):
        names.add(''.join(choice(chars) for i in range(r(min_len, max_len))))
    return names
        
def get_ballot_result(ballots):
    counter = {}
    for b in ballots:
        if b not in counter:
            counter[b] = 0
        counter[b] += 1
    if len(counter) == 1:
        return 'Informatiker des Jahres: ' + counter.keys()[0]
    else:
        res = [x for x in sorted(counter.items(), key = lambda x: -x[1])]
        winners = [x[0] for x in res if x[1] == res[0][1]]
        #print('winners:' + str(winners))
        if len(winners) == 1:
            return 'Informatiker des Jahres: ' + winners[0]
        else:
            return 'Unentschieden'

def create_test(min_depth=1, max_depth=10, n=4, length = 10):
    names = [x for x in gen_names()]
    ballots = [choice(names) for i in range(length)]
    return (ballots, [get_ballot_result(ballots)])

def main():
    parser = Parser(description='Generate test cases')
    parser.add_argument('-n', '--number', default=5, type=int, dest='n',\
        help = 'number of test cases to generate')
    parser.add_argument('-f', '--filename', default='test', type=str, dest='f',\
        help = 'base filename used')
    parser.add_argument('-p', '--path', default='secret', type=str, dest='p',\
        help = 'path to store generated test files (default secret)')

    args = parser.parse_args()
    
    cnt = args.n
    name = args.f
    testpath = args.p
    p = Path(testpath)
    if not p.exists():
        p.mkdir(parents=True)
    print(args)
    print(cnt, name, testpath)
    
    for i in range(cnt):
        (instring, outstring) = create_test(i, cnt, 4)
        filename = '{}/{:}_{:0>{width}}'.format(testpath, name, i, width=len(str(cnt - 1)))
        with open(filename + '.in', 'w') as infile:
            print(instring)
            infile.write('\n'.join(instring))
        with open(filename + '.ans', 'w') as outfile:
            print(outstring)
            outfile.write('\n'.join(outstring))

        #print(filename, p, h, d)
    pass

main()
