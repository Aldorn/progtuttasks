import test.TestObject;
import test.TestedClass;
import test.TestedProgram;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static test.matchers.UsedClassesMatcher.*;

import static test.Clazz.cs;

import org.junit.AfterClass;
import org.junit.Test;

public class T40Get {
	private final TestedClass TutorialArrayList = TestedProgram.getClass("edu.kit.informatik.util.TutorialArrayList");

	@Test
	public void get10() {
		TestObject list = TutorialArrayList.nev();
		for (int i = 0; i < 10; i++) {
			list.runVoid("add", cs(Object.class), "item" + i);
		}
		for (int i = 0; i < 10; i++) {
			assertThat(list.run(Object.class, "getAt", i), is("item" + i));
		}
	}

	@Test
	public void get40From5() {
		TestObject list = TutorialArrayList.nev(5);
		for (int i = 0; i < 40; i++) {
			list.runVoid("add", cs(Object.class), "item" + i);
		}
		for (int i = 0; i < 40; i++) {
			assertThat(list.run(Object.class, "getAt", i), is("item" + i));
		}
	}
	
	@Test
	public void getNonExistant() {
		TestObject list = TutorialArrayList.nev(5);
		for (int i = -5; i < 10; i++) {
			assertThat(list.run(Object.class, "getAt", i), is((Object) null));
		}
		for (int i = 0; i < 3; i++) {
			list.runVoid("add", cs(Object.class), "item" + i);
		}
		assertThat(list.run(Object.class, "getAt", 3), is((Object) null));
	}
	
	@AfterClass
	public static void checkUsedPackages() {
		assertThat(TestedProgram.code, onlyUses("java.lang"));
	}
}
