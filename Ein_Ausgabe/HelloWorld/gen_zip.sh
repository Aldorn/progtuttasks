#!/bin/bash

echo "generating problem zip file"

function usage {
    echo "options:"
    echo "-f    base filename"
    echo "-d    remove previous archives"
    echo "-h    show this help"
}

function gather_tests {
    echo "gathering test files..."
    find data/*/ -name '*.in' -exec zip $1 {} \;
    find data/*/ -name '*.ans' -exec zip $1 {} \;
}

FILE=problem
DELETE=0
while getopts "hdf:" OPT; do
    case $OPT in
    f )
        FILE=${OPTARG}
        ;;
    d)
        echo "Deleting previous file"
        DELETE=1
        ;;
    h ) usage
    exit 0;;
    \? )
        echo invalid option
        exit 1
    esac
done    

echo "Writing to $FILE.zip"

if [ $DELETE == 1 ]; then
    if [ -f ${FILE}.zip ]; then rm ${FILE}.zip; fi
    if [ -f ${FILE} ]; then rm ${FILE}; fi
fi

if [ -f domjudge-problem.ini ]; then
    echo "Adding domjudge-problem.ini..."
    zip $FILE domjudge-problem.ini
else
    echo "error: needs domjudge-problem.ini!"
    exit 2
fi

if [ -f problem.pdf ]; then
    echo "Adding problem.pdf..."
    zip $FILE problem.pdf
elif [ -f problem.html ]; then
    echo "Adding problem.html..."
    zip $FILE problem.html
elif [ -f problem.txt ]; then
    echo "Adding problem.txt..."
    zip $FILE problem.txt
elif [ -f problem.tex ];then
    echo "attemting to compile problem.tex..."
    if pdflatex -interaction=nonstopmode problem.tex; then
        echo "Success! Adding problem.pdf..."
        zip $FILE problem.pdf
    fi
else
    echo "WARNING: problem.{pdf, html, txt, tex} not found"
fi

gather_tests $FILE
