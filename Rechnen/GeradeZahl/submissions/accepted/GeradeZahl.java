/**
 * Musterlösung zur Aufgabe aus dem Tutorium. Berechnet, ob eine gegebene Zahl
 * gerade ist.
 * 
 * @author Moritz Halm
 *
 */
public class GeradeZahl {

	/**
	 * Mainmethode, wird beim Start des Programms ausgeführt.
	 * 
	 * @param args
	 *            Die Kommandozeilenargumente, die dem Programm beim Start
	 *            übergeben werden.
	 * 
	 */
	public static void main(String[] args) {
		// Bestimme erstes Kommandozeilenargument
		String arg1 = args[0];
		// Interpretiert die Zeichenkette als Ganzzhal mithilfe der
		
		// Bibliotheksfunktion Integer.parseInt()
		int x = Integer.parseInt(arg1);
		
		// Bestimme, ob x gerade ist (Divisionsrest mit 2 gleich 0)
		boolean isEven = (x % 2 == 0);
		// Alternative: Betrachte niederwertigstes Bit (Einer-Stelle im
		// Binärsystem)
		// boolean isEven = (x & 1 == 0)
		
		// Ausgabe
		System.out.println(isEven);

	}

}
