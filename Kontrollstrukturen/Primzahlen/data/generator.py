#!/usr/bin/python3

import math
from random import randint as r, choice
from argparse import ArgumentParser as Parser
from pathlib import Path

bool_lit = {True:'true', False:'false'}

def is_prime(x):
    if x < 2: return False
    if x == 2: return True
    if x % 2 == 0: return False
    for i in range(3, int(math.sqrt(x)) + 1, 2):
        if x % i == 0: return False
    return True

def create_test(i=0, n=4):
    x = r(1, 1000) if i != 0 else 1
    
    return (str(x), bool_lit[is_prime(x)])

def main():
    parser = Parser(description='Generate test cases')
    parser.add_argument('-n', '--number', default=10, type=int, dest='n',\
        help = 'number of test cases to generate')
    parser.add_argument('-f', '--filename', default='test', type=str, dest='f',\
        help = 'base filename used')
    parser.add_argument('-p', '--path', default='secret', type=str, dest='p',\
        help = 'path to store generated test files (default secret)')

    args = parser.parse_args()
    
    cnt = args.n
    name = args.f
    testpath = args.p
    p = Path(testpath)
    if not p.exists():
        p.mkdir(parents=True)
    print(args)
    print(cnt, name, testpath)
    
    for i in range(cnt):
        (instring, outstring) = create_test(i, cnt)
        filename = '{}/{:}_{:0>{width}}'.format(testpath, name, i, width=len(str(cnt - 1)))
        with open(filename + '.in', 'w') as infile:
            print(instring)
            infile.writelines(instring)
        with open(filename + '.ans', 'w') as outfile:
            print(outstring)
            outfile.write(outstring)

        #print(filename, p, h, d)
    pass

main()
