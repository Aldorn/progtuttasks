import static org.junit.Assert.assertThat;
import static test.TestedProgram.main;
import static test.matchers.CLIMatchers.prints;

import org.junit.Test;

import test.Input;

public class T20WallCheck {

	private static final String[] ALL_WALL_TYPES = {
		//@formatter:off
		"┌┬╴╶┐",
		"├┼──┤",
		"╵│  │",
		"╷│  │",
		"└┴──┘"
		//@formatter:on
	};

	private static String allWallTypeDef() {
		return Input.getFile(ALL_WALL_TYPES);
	}

	@Test
	public void canRead() {
		final String routeDef = Input.getFile("(0,0)");
		assertThat(main(allWallTypeDef(), routeDef), prints("(0,0)"));
	}

	@Test
	public void wallsPresent() {
		final String routeDef = Input.getFile(
			//@formatter:off
			"(2,1)↓↓↓↓↓↓→→→→→→↑↑↑↑↑↑↑↑←",
			"(2,2)↓↓↓↓↓↓→→→→→→↑←",
			"(-1,0)→→→←",
			"(-1,1)→→→←",
			"(-1,2)→←←",
			"(-1,3)→→→←",
			"(0,-1)↓↓↓↓↓↓↑",
			"(0,0)←←←↑↑↑↑→→→→→↓↓↓↓↓↓",
			"(0,1)←←←↑↑↑↑→→→→→",
			"(0,2)→→→←",
			"(0,3)←←←↓↓↓↓↓↓→→→→→",
			"(0,4)↑↑↑↑↓",
			"(1,-1)↓↓↓↓↓↓↑",
			"(1,0)↑↑↑↑←←←↓↓↓↓↓↓",
			"(1,1)↑↑↑↑←←←",
			"(1,2)←←←",
			"(1,3)←←←↓↓↓↓↓↓",
			"(1,4)↑↑↑↑↓",
			"(2,-1)↓↑↑",
			"(2,0)↓↓↓↓↓↓",
			"(2,3)↓↓↓↓↓↓",
			"(2,4)↑↑↑↑↓",
			"(3,-1)↓↓↓↓↓↓↑",
			"(3,0)↑↑↑↑→→→→→→↓↓↓↓↓↓",
			"(3,1)↑↑↑↑→→→→→→",
			"(3,2)→→→→→→",
			"(3,3)→→→→→→↓↓↓↓↓↓",
			"(3,4)↑↑↑↑↓",
			"(4,0)←←←←←←→",
			"(4,1)←←←←←←→",
			"(4,2)←←←←←←→",
			"(4,3)←←←←←←→"
			//@formatter:on
		);
		
		final String[] output = {
			//@formatter:off
			"(2,1)",
			"(2,2)",
			"(-2,0)",
			"(-2,1)",
			"(-2,2)",
			"(-2,3)",
			"(0,-2)",
			"(0,0)",
			"(0,1)",
			"(-1,2)",
			"(0,3)",
			"(0,5)",
			"(1,-2)",
			"(1,0)",
			"(1,1)",
			"(1,2)",
			"(1,3)",
			"(1,5)",
			"(2,-2)",
			"(2,0)",
			"(2,3)",
			"(2,5)",
			"(3,-2)",
			"(3,0)",
			"(3,1)",
			"(3,2)",
			"(3,3)",
			"(3,5)",
			"(5,0)",
			"(5,1)",
			"(5,2)",
			"(5,3)"
			//@formatter:on
		};
		assertThat(main(allWallTypeDef(), routeDef), prints(output));
	}
}
