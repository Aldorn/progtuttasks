import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static test.Clazz.cs;
import static test.matchers.UsedClassesMatcher.onlyUses;

import org.junit.AfterClass;
import org.junit.Test;

import test.TestObject;
import test.TestedClass;
import test.TestedProgram;

public class T70Remove {
	private final TestedClass TutorialArrayList = TestedProgram.getClass("edu.kit.informatik.util.TutorialArrayList");

	@Test
	public void remove100() {
		TestObject list = TutorialArrayList.nev();
		for (int i = 0; i < 100; i++) {
			list.runVoid("add", cs(Object.class), "item" + i);
		}
		for (int i = 99; i >= 0; i--) {
			assertThat(list.run(boolean.class, "remove", cs(Object.class), "item" + i), is(true));
			assertThat(list.run(boolean.class, "remove", cs(Object.class), "item" + i), is(false));
			if (i <= 5) {
				assertThat(list.run(int.class, "getReservedSize"), is(10));
			} else if (i <= 10) {
				assertThat(list.run(int.class, "getReservedSize"), is(20));
			} else if (i <= 20) {
				assertThat(list.run(int.class, "getReservedSize"), is(40));
			} else if (i <= 40) {
				assertThat(list.run(int.class, "getReservedSize"), is(80));
			} else {
				assertThat(list.run(int.class, "getReservedSize"), is(160));
			}
			assertThat(list.run(int.class, "getSize"), is(i));
		}
	}
	
	@Test
	public void remove400By50() {
		TestObject list = TutorialArrayList.nev(2);
		for (int i = 0; i < 400; i++) {
			list.runVoid("add", cs(Object.class), "item" + i / 50);
		}
		for (int i = 0; i < 8; i++) {
			assertThat(list.run(boolean.class, "remove", cs(Object.class), "item" + i), is(true));
			int size = 400 - (i + 1) * 50;
			if (size <= 1) {
				assertThat(list.run(int.class, "getReservedSize"), is(2));
			} else if (size <= 32) {
				assertThat(list.run(int.class, "getReservedSize"), is(64));
			} else if (size <= 64) {
				assertThat(list.run(int.class, "getReservedSize"), is(128));
			} else if (size <= 128) {
				assertThat(list.run(int.class, "getReservedSize"), is(256));
			} else {
				assertThat(list.run(int.class, "getReservedSize"), is(512));
			}
			assertThat(list.run(boolean.class, "remove", cs(Object.class), "item" + i * 50), is(false));
			if (size <= 1) {
				assertThat(list.run(int.class, "getReservedSize"), is(2));
			} else if (size <= 32) {
				assertThat(list.run(int.class, "getReservedSize"), is(64));
			} else if (size <= 64) {
				assertThat(list.run(int.class, "getReservedSize"), is(128));
			} else if (size <= 128) {
				assertThat(list.run(int.class, "getReservedSize"), is(256));
			} else {
				assertThat(list.run(int.class, "getReservedSize"), is(512));
			}
			assertThat(list.run(int.class, "getSize"), is(400 - (i + 1) * 50));
		}
	}
	
	@Test
	public void remove100InBetween() {
		TestObject list = TutorialArrayList.nev(20);
		for (int i = 0; i < 100; i++) {
			list.runVoid("add", cs(Object.class), "item" + i);
		}
		for (int i = 99; i >= 5; i--) {
			list.runVoid("removeAt", i / 2);
			if (i <= 10) {
				assertThat(list.run(int.class, "getReservedSize"), is(20));
			} else if (i <= 20) {
				assertThat(list.run(int.class, "getReservedSize"), is(40));
			} else if (i <= 40) {
				assertThat(list.run(int.class, "getReservedSize"), is(80));
			} else {
				assertThat(list.run(int.class, "getReservedSize"), is(160));
			}
			assertThat(list.run(int.class, "getSize"), is(i));
		}
		for (int i = 0; i <= 1; i++) {
			assertThat(list.run(Object.class, "getAt", i), is("item" + i));
		}
		for (int i = 4; i >= 2; i--) {
			assertThat(list.run(Object.class, "getAt", i), is("item" + (95 + i)));
		}
	}
	
	@AfterClass
	public static void checkUsedPackages() {
		assertThat(TestedProgram.code, onlyUses("java.lang"));
	}
}
