#!/usr/bin/python3

import math
from random import randint
from argparse import ArgumentParser as Parser
from pathlib import Path

def r():
    return randint(0, 10000)

def create_test():
    pos = (r(), r())
    h = (r(), r())
    diff = tuple((abs(x1 - x2) for x1, x2 in zip(pos, h)))
    return (pos, h, diff[0] + diff[1])

def main():
    parser = Parser(description='Generate test cases')
    parser.add_argument('-n', '--number', default=16, type=int, dest='n',\
        help = 'number of test cases to generate')
    parser.add_argument('-f', '--filename', default='test', type=str, dest='f',\
        help = 'base filename used')
    parser.add_argument('-p', '--path', default='secret', type=str, dest='p',\
        help = 'path to store generated test files (default secret)')

    args = parser.parse_args()
    
    cnt = args.n
    name = args.f
    testpath = args.p
    p = Path(testpath)
    if not p.exists():
        p.mkdir(parents=True)
    print(args)
    print(cnt, name, testpath)
    
    for i in range(cnt):
        (p, h, d) = create_test()
        filename = '{}/{:}_{:0>{width}}'.format(testpath, name, i, width=len(str(cnt - 1)))
        with open(filename + '.in', 'w') as infile:
            lines = ['{0[0]} {0[1]}  '.format(x) for x in [p, h]]
            infile.writelines(lines)
        with open(filename + '.ans', 'w') as outfile:
            outlines = ['{}\n'.format(str(d))]
            outfile.writelines(outlines)

        #print(filename, p, h, d)
    pass

main()
