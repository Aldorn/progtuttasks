import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static test.Clazz.cs;
import static test.matchers.UsedClassesMatcher.onlyUses;

import org.junit.AfterClass;
import org.junit.Test;

import test.TestObject;
import test.TestedClass;
import test.TestedProgram;

public class T60RemoveAt {
	private final TestedClass TutorialArrayList = TestedProgram.getClass("edu.kit.informatik.util.TutorialArrayList");

	@Test
	public void remove100FromEnd() {
		TestObject list = TutorialArrayList.nev();
		for (int i = 0; i < 100; i++) {
			list.runVoid("add", cs(Object.class), "item" + i);
		}
		for (int i = 99; i >= 0; i--) {
			list.runVoid("removeAt", i);
			if (i <= 5) {
				assertThat(list.run(int.class, "getReservedSize"), is(10));
			} else if (i <= 10) {
				assertThat(list.run(int.class, "getReservedSize"), is(20));
			} else if (i <= 20) {
				assertThat(list.run(int.class, "getReservedSize"), is(40));
			} else if (i <= 40) {
				assertThat(list.run(int.class, "getReservedSize"), is(80));
			} else {
				assertThat(list.run(int.class, "getReservedSize"), is(160));
			}
			assertThat(list.run(int.class, "getSize"), is(i));
		}
	}
	
	@Test
	public void remove100FromStart() {
		TestObject list = TutorialArrayList.nev(2);
		for (int i = 0; i < 100; i++) {
			list.runVoid("add", cs(Object.class), "item" + i);
		}
		for (int i = 99; i >= 5; i--) {
			list.runVoid("removeAt", 0);
			if (i <= 8) {
				assertThat(list.run(int.class, "getReservedSize"), is(16));
			} else if (i <= 16) {
				assertThat(list.run(int.class, "getReservedSize"), is(32));
			} else if (i <= 32) {
				assertThat(list.run(int.class, "getReservedSize"), is(64));
			} else {
				assertThat(list.run(int.class, "getReservedSize"), is(128));
			}
			assertThat(list.run(int.class, "getSize"), is(i));
		}
		for (int i = 4; i >= 0; i--) {
			assertThat(list.run(Object.class, "getAt", i), is("item" + (95 + i)));
		}
	}
	
	@Test
	public void remove100InBetween() {
		TestObject list = TutorialArrayList.nev(20);
		for (int i = 0; i < 100; i++) {
			list.runVoid("add", cs(Object.class), "item" + i);
		}
		for (int i = 99; i >= 5; i--) {
			list.runVoid("removeAt", i / 2);
			if (i <= 10) {
				assertThat(list.run(int.class, "getReservedSize"), is(20));
			} else if (i <= 20) {
				assertThat(list.run(int.class, "getReservedSize"), is(40));
			} else if (i <= 40) {
				assertThat(list.run(int.class, "getReservedSize"), is(80));
			} else {
				assertThat(list.run(int.class, "getReservedSize"), is(160));
			}
			assertThat(list.run(int.class, "getSize"), is(i));
		}
		for (int i = 0; i <= 1; i++) {
			assertThat(list.run(Object.class, "getAt", i), is("item" + i));
		}
		for (int i = 4; i >= 2; i--) {
			assertThat(list.run(Object.class, "getAt", i), is("item" + (95 + i)));
		}
	}
	
	@AfterClass
	public static void checkUsedPackages() {
		assertThat(TestedProgram.code, onlyUses("java.lang"));
	}
}
