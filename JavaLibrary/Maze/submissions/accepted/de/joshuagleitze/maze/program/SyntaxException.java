package de.joshuagleitze.maze.program;

import java.util.Scanner;

public class SyntaxException extends MazeException {

	private static final long serialVersionUID = -8738805486696047641L;
	protected long lineNumber;
	protected long charPosition;
	protected String expected;
	protected String actual;

	protected SyntaxException() {
	}

	public SyntaxException(String expected, String actual) {
		this.expected = expected;
		this.actual =  "'" + actual + "'";
	}
	
	public SyntaxException(String expected, char actual) {
		this(expected, Character.toString(actual));
	}

	public SyntaxException(String expected, Scanner scanner) {
		this.expected = expected;
		this.actual = scanner.hasNext() ? "'" + scanner.next() + "'" : "end of input";
		this.readCharPositionFromScanner(scanner);
	}

	public SyntaxException(String expected, String actual, Scanner scanner) {
		this(expected, actual);
		this.readCharPositionFromScanner(scanner);
	}

	public void setLineNumber(long lineNumber) {
		this.lineNumber = lineNumber;
	}

	public void setCharacterPosition(long charNumber) {
		this.charPosition = charNumber;
	}

	public void readCharPositionFromScanner(Scanner scanner) {
		this.charPosition = scanner.match().start() + 1;
	}

	@Override
	public String getMessage() {
		return String.format("syntax error in %s, line %d, character %d: %s", fileName, lineNumber, charPosition,
				expectation());
	}

	protected String expectation() {
		return String.format("expected %s, but found %s", this.expected, this.actual);
	}
}
