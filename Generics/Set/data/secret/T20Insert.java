import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.is;
import static test.Clazz.cs;

import org.junit.AfterClass;
import org.junit.Test;

import test.TestedClass;
import test.TestedProgram;
import test.TestObject;

public class T20Insert {
	private final TestedClass mSet = TestedProgram.getClass("edu.kit.informatik.MySet");
	
	@Test
	public void testInsert() {
		TestObject obj = mSet.nev();
		for (int i = 0; i < 10; ++i) {
			assertThat(obj.run(Boolean.class, "insert", cs(Object.class), i), is(true));
		}
		assertThat(obj.run(Integer.class, "size"), is(10));
		for (int i = 0; i < 10; ++i) {
			assertThat(obj.run(Boolean.class, "insert", cs(Object.class), i), is(false));
		}
		assertThat(obj.run(Integer.class, "size"), is(10));
	}
	
	@Test
	public void testContains() {
		TestObject obj = mSet.nev();
		for (int i = 0; i < 10; ++i) {
			assertThat(obj.run(Boolean.class, "insert", cs(Object.class), i), is(true));
		}
		assertThat(obj.run(Integer.class, "size"), is(10));
		for (int i = 0; i < 10; ++i) {
			assertThat(obj.run(Boolean.class, "contains", cs(Object.class), i), is(true));
		}
	}
	
	@Test
	public void testRemove() {
		TestObject obj = mSet.nev();
		for (int i = 0; i < 10; ++i) {
			assertThat(obj.run(Boolean.class, "insert", cs(Object.class), i), is(true));
		}
		assertThat(obj.run(Integer.class, "size"), is(10));
		for (int i = 0; i < 10; ++i) {
			assertThat(obj.run(Boolean.class, "remove", cs(Object.class), i), is(true));
		}
		assertThat(obj.run(Integer.class, "size"), is(0));
	}
	
}
