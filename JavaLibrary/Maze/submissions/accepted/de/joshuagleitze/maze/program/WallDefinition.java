package de.joshuagleitze.maze.program;

import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import static de.joshuagleitze.maze.Direction.*;

import de.joshuagleitze.maze.Direction;

public enum WallDefinition {
	NONE(' '),
	LEFT('╴', WEST),
	TOP('╵', NORTH),
	RIGHT('╶', EAST),
	BOTTOM('╷', SOUTH),
	LEFTRIGHT('─', EAST, WEST),
	TOPBOTTOM('│', NORTH, SOUTH),
	RIGHTBOTTOM('┌', EAST, SOUTH),
	LEFTBOTTOM('┐', WEST, SOUTH),
	RIGHTTOP('└', EAST, NORTH),
	LEFTTOP('┘', WEST, NORTH),
	RIGHTTOPBOTTOM('├', EAST, NORTH, SOUTH),
	LEFTTOPBOTTOM('┤', WEST, NORTH, SOUTH),
	LEFTRIGHTBOTTOM('┬', WEST, EAST, SOUTH),
	LEFTRIGHTTOP('┴', WEST, EAST, NORTH),
	ALL('┼', NORTH, EAST, SOUTH, WEST);

	private final Set<Direction> definedWalls = EnumSet.noneOf(Direction.class);
	private final char character;
	private static final Map<Character, WallDefinition> CHAR_MAP = charMap();

	private WallDefinition(char character, Direction... walls) {
		this.definedWalls.addAll(Arrays.asList(walls));
		this.character = character;
	}

	private static Map<Character, WallDefinition> charMap() {
		WallDefinition[] values = values();
		final Map<Character, WallDefinition> map = new HashMap<>(values.length, 1);
		for (WallDefinition constant : values) {
			map.put(constant.character, constant);
		}
		return map;
	}

	public Set<Direction> getDefinedWalls() {
		return Collections.unmodifiableSet(this.definedWalls);
	}

	public static boolean isDefinitionChar(char character) {
		return CHAR_MAP.containsKey(character);
	}

	public static WallDefinition fromChar(char character) {
		return Optional.ofNullable(CHAR_MAP.get(character))
				.orElseThrow(() -> new IllegalArgumentException(character + "is not a wall definition character."));
	}
	
	public char toChar() {
		return this.character;
	}
}
