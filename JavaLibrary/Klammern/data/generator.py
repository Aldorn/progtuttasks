#!/usr/bin/python3

import math
from random import randint as r, choice
from argparse import ArgumentParser as Parser
from pathlib import Path

parentheses = [('(', ')'), ('[', ']')]
closing = {'(' : ')', '[' : ']'}

def gen_parentheses(depth):
    if r(1, 3) == 4 or depth == 0: return ''
    p = choice(parentheses)
    return p[0] + gen_parentheses(depth - 1) + p[1] + gen_parentheses(depth - 1)

def validate_parentheses(s):
    stack = []
    for p in s:
        if p in closing:
            stack.append(closing[p])
        else: 
            if not len(stack) or stack.pop() != p:
                return False
    return len(stack) == 0

yes_no = {True : 'Ja', False : 'Nein'}

def create_test(min_depth=1, max_depth=10, n=4):
    inputs = [gen_parentheses(r(min_depth, max_depth)) for i in range(n - n // 2)]
    inputs += [''.join(choice('()[]') for i in range(r(1, 3 ** max_depth))) for i in range(n // 2)]
    return (inputs, [yes_no[validate_parentheses(p)] for p in inputs])

def main():
    parser = Parser(description='Generate test cases')
    parser.add_argument('-n', '--number', default=5, type=int, dest='n',\
        help = 'number of test cases to generate')
    parser.add_argument('-f', '--filename', default='test', type=str, dest='f',\
        help = 'base filename used')
    parser.add_argument('-p', '--path', default='secret', type=str, dest='p',\
        help = 'path to store generated test files (default secret)')

    args = parser.parse_args()
    
    cnt = args.n
    name = args.f
    testpath = args.p
    p = Path(testpath)
    if not p.exists():
        p.mkdir(parents=True)
    print(args)
    print(cnt, name, testpath)
    
    for i in range(cnt):
        (instring, outstring) = create_test(i, cnt, 4)
        filename = '{}/{:}_{:0>{width}}'.format(testpath, name, i, width=len(str(cnt - 1)))
        with open(filename + '.in', 'w') as infile:
            print(instring)
            infile.write('\n'.join(instring))
        with open(filename + '.ans', 'w') as outfile:
            print(outstring)
            outfile.write('\n'.join(outstring))

        #print(filename, p, h, d)
    pass

main()
