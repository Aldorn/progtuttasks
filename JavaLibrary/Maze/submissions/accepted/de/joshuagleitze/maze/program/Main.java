package de.joshuagleitze.maze.program;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;

import de.joshuagleitze.maze.Maze;

public class Main {
	public static void main(String[] args) {
		if (args.length < 2) {
			System.err.println("bad arguments");
			return;
		}
		final Path mazeDefinitionFile = Paths.get(args[0]);
		final Path routeDefinitionFile = Paths.get(args[1]);
		final Maze maze;
		try {
			try {
				maze = new MazeFromFileBuilder(mazeDefinitionFile).build();
			} catch (MazeException error) {
				error.setFileName("maze definition");
				throw error;
			}
			try {
				makeMoves(routeDefinitionFile, maze);
			} catch (MazeException error) {
				error.setFileName("route definition");
				throw error;
			}

		} catch (MazeException e) {
			System.err.println(e.getMessage());
			return;
		}
	}

	private static void makeMoves(Path moveDefinitionFile, Maze maze) throws MazeException {
		int i = 1;
		try (final BufferedReader lineReader = Files.newBufferedReader(moveDefinitionFile)) {
			for (String line = lineReader.readLine(); line != null; line = lineReader.readLine()) {
				DamnedMover mover = new DamnedMover(line, maze);
				mover.move();
				if (mover.getDamned().isPositioned()) {
					System.out.println(mover.getDamned().getPosition());
				} else {
					System.out.println("lucky");
				}
				i++;
			}
		} catch (NoSuchFileException | FileNotFoundException fileNotFound) {
			throw new DefinitionFileNotFoundException();
		} catch (IOException ioError) {
			throw new DefinitionFileNotReadableException(ioError);
		} catch(SyntaxException syntaxError) {
			syntaxError.setLineNumber(i);
			throw syntaxError;
		}
	}
}
