package de.joshuagleitze.maze.program;

public class DefinitionFileNotReadableException extends MazeException {

	private static final long serialVersionUID = 5537329307709880972L;

	public DefinitionFileNotReadableException(Throwable cause) {
		super(cause);
	}
	
	@Override
	public String getMessage() {
		return String.format("%s not readable: %s", this.fileName, this.getCause().getMessage());
	}
}
