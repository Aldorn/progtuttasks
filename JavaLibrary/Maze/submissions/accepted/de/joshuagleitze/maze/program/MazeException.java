package de.joshuagleitze.maze.program;

public abstract class MazeException extends Exception {
	private static final long serialVersionUID = -743695844312837196L;
	protected String fileName;

	public MazeException(Throwable cause) {
		super(cause);
	}

	public MazeException(String fileName, Throwable cause) {
		this(cause);
		setFileName(fileName);
	}

	protected MazeException() {
	}

	public void setFileName(String name) {
		this.fileName = name;
	}

}
