#!/usr/bin/python3

import math
from random import randint, choice
from argparse import ArgumentParser as Parser
from pathlib import Path

def r():
    return randint(0, 10000)

chars = [chr(c) for c in range(ord('a'), ord('z'))] + \
        [chr(c) for c in range(ord('A'), ord('Z'))] + \
        [chr(c) for c in range(ord('0'), ord('9'))] + ['_']

def gen_name(l, chars):
    return "".join((choice(chars) for i in range(l)))

def create_test(i=0):
    l = randint(3, 80)
    name = "{}.{}".format(gen_name(l, chars + [' ']), gen_name(3, chars))
    instring = '"{}"\n'.format(name)
    outstring = '"C:\\Windows\\System32\\{}"\n'.format(name)
    return (instring, outstring)

def main():
    parser = Parser(description='Generate test cases')
    parser.add_argument('-n', '--number', default=5, type=int, dest='n',\
        help = 'number of test cases to generate')
    parser.add_argument('-f', '--filename', default='test', type=str, dest='f',\
        help = 'base filename used')
    parser.add_argument('-p', '--path', default='secret', type=str, dest='p',\
        help = 'path to store generated test files (default secret)')

    args = parser.parse_args()
    
    cnt = args.n
    name = args.f
    testpath = args.p
    p = Path(testpath)
    if not p.exists():
        p.mkdir(parents=True)
    print(args)
    print(cnt, name, testpath)
    
    for i in range(cnt):
        (instring, outstring) = create_test(i)
        filename = '{}/{:}_{:0>{width}}'.format(testpath, name, i, width=len(str(cnt - 1)))
        with open(filename + '.in', 'w') as infile:
            print(instring)
            infile.writelines(instring)
        with open(filename + '.ans', 'w') as outfile:
            print(outstring)
            outfile.write(outstring)

        #print(filename, p, h, d)
    pass

main()
