/** Contains method to compare two fractions.
* @author Annika Berger
*/
public class Compare {

	/** Build fractions a/b and c/d and compares if they are equal. Prints the 		* result.
	* @param args four numbers a b c d in floating point notation
	*/
	public static void main(String[] args) {
		/* Parsing of the String arguments into Doubles (only possible because it is promised that only numbers are given as input).
		*/
		double a = Double.parseDouble(args[0]);
		double b = Double.parseDouble(args[1]);
		double c = Double.parseDouble(args[2]);
		double d = Double.parseDouble(args[3]);

		// fractions
		double leftside = a/b;
		double rightside = c/d;
		
		/* Comparision using Math.abs and an epsilon (have a look at Lecture Slides "Types and Variables" Slide 15) to avoid problems because of rounding errors. 
		*/ 
		boolean same = Math.abs(leftside - rightside) < 0.1E-8;

		// Print out result
		System.out.println(same);
	}
}
