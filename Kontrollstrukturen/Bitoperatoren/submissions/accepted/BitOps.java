import java.util.*;
public class BitOps {

	public static String repeat(String s, int n) {
		return String.join("", Collections.nCopies(n, s));
	}

	public static String toBinary(int n) {
		String res = "";
		while(n != 0) {
			res = String.valueOf(n % 2) + res;
			n /= 2;
		}
		return repeat("0", 32 - res.length()) + res;
	}

	public static void main(String[] args) {
		int x = Integer.parseInt(args[0]);
		int y = Integer.parseInt(args[2]);
		int res;
		switch(args[1]) {
		case "&":
			res = x & y;
			break;
		case "|":
			res = x | y;
			break;
		case "^":
			res = x ^ y;
			break;
		default:
			throw new IllegalArgumentException(args[1]);
        }
		System.out.println(toBinary(x));
		System.out.println(toBinary(y));
		System.out.println(repeat(args[1], 32));
		System.out.println(toBinary(res));
	}
}
		
