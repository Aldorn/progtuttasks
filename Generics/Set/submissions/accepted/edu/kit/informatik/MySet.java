package edu.kit.informatik;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MySet<T> implements Iterable<T> {
	private class ListItem {
		public ListItem(T data) {
			this.data = data;
		}
		ListItem next;
		T data;
	}
	
	private class SetIterator implements Iterator<T> {
		private ListItem itm;
		private ListItem prev;
		
		public SetIterator(ListItem start) {
			itm = start;
		}
		
		@Override
		public T next() throws NoSuchElementException {
			if (itm == null) throw new NoSuchElementException();
			T data = itm.data;
			prev = itm;
			itm = itm.next;
			return prev.data;
		}

		@Override
		public boolean hasNext() {
			return itm != null;
		}
		
		@Override
		public void remove() {
			if (itm == null) throw new NoSuchElementException();
			if (prev == null) {
				first = itm.next;
				itm = itm.next;
			} else if (itm != null) {
				prev.next = itm.next;
				itm = itm.next;
			}
		}
	}
	
	private ListItem first;
	
	public MySet() {
		first = null;
	}
	
	private static <T> boolean compare(T x, T y) {
		return (x == null && y == null) || (x != null && x.equals(y)); 
	}
	
	public boolean insert(T x) {
		if (first == null) {
			first = new ListItem(x);
			return true;
		} else {
			ListItem itm = first;
			ListItem prev = null;
			while (itm != null) {
				if (compare(x, itm.data)) {
					return false;
				} else {
					prev = itm;
					itm = itm.next;
				}
			}
			prev.next = new ListItem(x);
			return true;
		}
	}
	
	public boolean contains(T x) {
		ListItem itm = first;
		while (itm != null) {
			if (compare(x, itm.data)) {
				return true;
			}
			itm = itm.next;
		}
		return false;
	}
	
	public boolean remove(T x) {
		ListItem itm = first;
		ListItem prev = null;
		while (itm != null) {
			if (compare(x, itm.data)) {
				break;
			}
			prev = itm;
			itm = itm.next;
		}
		if (itm == null) {
			return false;
		} else if (itm == first) {
			first = itm.next;
		} else {
			prev.next = itm.next;
		}
		return true;
	}
	
	public int size() {
		Iterator i = iterator();
		int count = 0;
		while (i.hasNext()) {
			count++;
			i.next();
		}
		return count;
	}
	
	public Iterator<T> iterator() {
		return new SetIterator(first);
	}
}
