package edu.kit.informatik.util;

public class TutorialArrayList {
	
	private Object[] array;
	private int firstFreeIndex;
	private final int minSize;
	
	public TutorialArrayList() {
		this(10);
	}

	public TutorialArrayList(int size) {
		minSize = size;
		array = new Object[size];
	}
	
	public void add(Object item) {
		if (firstFreeIndex >= array.length) {
			enlarge();
		}
		array[firstFreeIndex++] = item;
	}
	
	public void insertAt(Object item, int index) {
		if (firstFreeIndex < index || index < 0) {
			return;
		}
		if (firstFreeIndex >= array.length) {
			final Object[] newArray = new Object[2 * array.length];
			int i, j;
			for (i = 0, j = 0; j < firstFreeIndex; i++, j++) {
				if (i == index) {
					i++;
				}
				newArray[i] = array[j];
			}
			array = newArray;
		} else {
			for (int i = firstFreeIndex; i > index; i--) {
				array[i] = array[i - 1];
			}
		}
		array[index] = item;
		firstFreeIndex++;
	}
	
	public Object getAt(int index) {
		if (index < 0 || index >= firstFreeIndex) {
			return null;
		}
		return array[index];
	}
	
	public int getReservedSize() {
		return array.length;
	}
	
	public boolean remove(Object item) {
		boolean removed = false;
		int i = 0;
		while (i < firstFreeIndex) {
			if ((item == null && array[i] == null) || (item != null && item.equals(array[i]))) {
				removed = true;
				this.removeAt(i);
			} else {
				i++;
			}
		}
		return removed;
	}
	
	public void removeAt(int index) {
		if (firstFreeIndex <= index && index < 0) {
			return;
		}
		firstFreeIndex--;
		if (4 * firstFreeIndex <= array.length && array.length / 2 >= minSize) {
			Object[] newArray = new Object[array.length / 2];
			int i, j;
			for (i = 0, j = 0; i < firstFreeIndex; i++, j++) {
				if (j == index) {
					j++;
				}
				newArray[i] = array[j];
			}
			array = newArray;
		} else {
			for (int i = index; i < firstFreeIndex; i++) {
				array[i] = array[i + 1];
			}
		}
	}
	
	public int getSize() {
		return this.firstFreeIndex;
	}
	
	private void enlarge() {
		Object[] newList = new Object[array.length * 2];
		for (int i = 0; i < array.length; i++) {
			newList[i] = array[i];
		}
		array = newList;
	}
}
