import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static test.Clazz.cs;
import static test.matchers.UsedClassesMatcher.onlyUses;

import org.junit.AfterClass;
import org.junit.Test;

import test.TestObject;
import test.TestedClass;
import test.TestedProgram;

public class T50Insert {
	private final TestedClass TutorialArrayList = TestedProgram.getClass("edu.kit.informatik.util.TutorialArrayList");

	@Test
	public void insertInOrder100() {
		TestObject list = TutorialArrayList.nev();
		for (int i = 0; i < 100; i++) {
			list.runVoid("insertAt", cs(Object.class, int.class), "item" + i, i);
			if (i >= 80) {
				assertThat(list.run(int.class, "getReservedSize"), is(160));
			} else if (i >= 40) {
				assertThat(list.run(int.class, "getReservedSize"), is(80));
			} else if (i >= 20) {
				assertThat(list.run(int.class, "getReservedSize"), is(40));
			} else if (i >= 10) {
				assertThat(list.run(int.class, "getReservedSize"), is(20));
			}
		}
	}
	
	@Test
	public void insertInOrder100From1() {
		TestObject list = TutorialArrayList.nev(1);
		int potence = 0;
		for (int i = 0; i < 100; i++) {
			list.runVoid("insertAt", cs(Object.class, int.class), "item" + i, i);
			if (i >= Math.pow(2, potence)) {
				potence++;
			}
			assertThat(list.run(int.class, "getReservedSize"), is((int) Math.pow(2, potence)));
		}
	}
	
	@Test
	public void insertOutOfOrder() {
		TestObject list = TutorialArrayList.nev();
		for (int i = 0; i < 10; i++) {
			list.runVoid("insertAt", cs(Object.class, int.class), "item" + i, i);
		}
		list.runVoid("insertAt", cs(Object.class, int.class), "farAway", 100);
		list.runVoid("insertAt", cs(Object.class, int.class), "farAway", -1);
		assertThat(list.run(int.class, "getReservedSize"), is(10));
		assertThat(list.run(Object.class, "getAt", -1), is((Object) null));
		assertThat(list.run(Object.class, "getAt", 100), is((Object) null));
		for (int i = 0 ; i < 10; i++) {
			assertThat(list.run(Object.class, "getAt", i), is("item" + i));
		}
	}
	
	@Test
	public void insertInBetween50() {
		TestObject list = TutorialArrayList.nev();
		for (int j = 1; j <= 5; j++) {
			for (int i = 0; i < 10; i++) {
				list.runVoid("insertAt", cs(Object.class, int.class), "item" + i, i * j);
			}
		}
		assertThat(list.run(int.class, "getReservedSize"), is(80));
		for (int i = 0; i < 50; i++) {
			assertThat(list.run(Object.class, "getAt", i), is("item" + i / 5));
		}
	}
	
	@AfterClass
	public static void checkUsedPackages() {
		assertThat(TestedProgram.code, onlyUses("java.lang"));
	}
}
