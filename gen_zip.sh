#!/bin/bash

function usage {
    echo "options:"
    echo "-d    remove previously created archives"
    echo "-h    show this help"
    echo "-p P  where to search for problems"
    echo "-c    clean up. No files will be generated, except -p is specified"
}

DELETE=0
#CLEANUP=0
#BASE_DIR=.
#TEST=0

while getopts "hdp:ct" OPT; do
    case $OPT in
    d)
        echo "Deleting previous file"
        DELETE=1
        ;;
    h ) usage
    exit 0;;
    p )
        BASE_DIR=${OPTARG}
    ;;
    c )
        CLEANUP=1
    ;;
    t ) #dry run, don't touch anything
        TEST=1
    ;;
    \? )
        echo "unknown option ${OPTARG}"
        exit 1
    esac
done    

function pack_problem {
    BASE=$(readlink -f "$1")
    PROBLEM_DIR="$(pwd)"
    PROBLEMPATH="$(realpath --relative-to="$PROBLEM_DIR" "$BASE")"
    ZIP="$PROBLEM_DIR/$(basename "$BASE").zip"
    

    echo "Writing $PROBLEMPATH to $(realpath --relative-to="$PROBLEM_DIR" "$ZIP")"

    if [[ -n $TEST ]]; then
        echo pwd=$(pwd)
        echo "\$1    = \""$1"\""
        echo "\$BASE = \""$BASE"\""
        echo "\$ZIP  = \""$ZIP"\""
        echo "$PROBLEM_DIR"
        return
    fi

    cd "$BASE"

    if [[ -n $DELETE ]]; then
        if [ -f "$ZIP.zip" ]; then rm "$ZIP.zip"; fi
        if [ -f "$ZIP" ]; then rm "$ZIP"; fi
    fi

    if [[ -f domjudge-problem.ini ]]; then
        echo "Adding domjudge-problem.ini..."
        zip "$ZIP" domjudge-problem.ini
    else
        echo "error: needs domjudge-problem.ini!"
        exit 2
    fi

    if [[ -f problem.tex ]]; then
        echo "attemting to compile problem.tex..."
        if pdflatex -interaction=nonstopmode "$BASE/problem.tex" > /dev/null; then
            echo "Success! Adding problem.pdf..."
            zip "$ZIP" problem.pdf
        else
        	echo "Compiling $PROBLEMPATH/problem.tex failed!"
        	exit 1
        fi
    elif [[ -f problem.pdf ]]; then
        echo "Adding problem.pdf..."
        zip "$ZIP" problem.pdf
    elif [[ -f problem.html ]]; then
        echo "Adding problem.html..."
        zip "$ZIP" problem.html
    elif [[ -f problem.txt ]]; then
        echo "Adding problem.txt..."
        zip "$ZIP" problem.txt
    else
        echo "WARNING: problem.{pdf, html, txt, tex} not found"
    fi
    
    echo "gathering test files..."
    find data/*/ -name '*.in' -exec zip "$ZIP" {} \;
    find data/*/ -name '*.ans' -exec zip "$ZIP" {} \;
	find data/*/ -name '*.java' | while read file
	do
		base=${file%.*}
		cp $file $base.in
		touch $base.ans
		zip "$ZIP" $base.in
		zip "$ZIP" $base.ans
		rm $base.in $base.ans
	done

    cd "$PROBLEM_DIR"
}
if [[ -n $CLEANUP ]]; then
    echo "Cleaning all generated zip files"
    find . -type f -name "domjudge-problem.ini" -print0 | while IFS= read -r -d $'\0' line; do
        problem="$(dirname "$line")"
        if [[ -f "./$(basename $problem).zip" ]]; then
            echo "removing $(basename $problem).zip"
            if [[ -z $TEST ]]; then
                rm "./$(basename $problem).zip"
            fi
        fi
    done
fi

if [[ -z $CLEANUP || -n $BASE_DIR ]]; then
    echo "Generating zip files from $(readlink -f "$BASE_DIR")"
    find $BASE_DIR -type f -name "domjudge-problem.ini" -print0 | while IFS= read -r -d $'\0' line; do
        problem="$(dirname "$line")"
        echo "Problem detected: $(basename $problem) in $problem"
        pack_problem $problem
    done
#    for problem in ./*/ ; do 
#        echo "Packing problem $problem"
#        if [[ -d "$problem" && -f "$problem/domjudge-problem.ini" ]]; then
#            pack_problem "$problem"
#        fi; 
#    done
#else 
#    if  [[ -d $PROBLEM && -f "$PROBLEM/domjudge-problem.ini" ]]; then
#        echo "Packing single problem $PROBLEM"
#        pack_problem "$PROBLEM"
#    else
#        echo "problem not found"
#        exit 3;
#    fi
fi
