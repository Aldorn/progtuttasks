package de.joshuagleitze.maze.program;

import de.joshuagleitze.maze.Coordinate;
import de.joshuagleitze.maze.Direction;
import de.joshuagleitze.maze.MazeCell;

public class Damned {

	private MazeCell position;
	private boolean isPositioned = false;

	public void startOn(MazeCell start) {
		if (this.moveTo(start)) {
			this.isPositioned = true;
		}
	}

	private boolean moveTo(MazeCell newCell) {
		if (!newCell.isVisited()) {
			newCell.setVisited();
			this.position = newCell;
			return true;
		}
		return false;
	}

	public Coordinate getPosition() {
		checkPositioned();
		return position.getPosition();
	}

	public void move(Direction direction) {
		this.checkPositioned();
		MazeCell nextCell = this.position.getNeigbour(direction);
		if (this.position.hasWallTo(direction)) {
			return;
		}
		MazeCell oldCell = this.position;
		if (this.moveTo(nextCell)) {
			oldCell.setUnvisited();
		}
	}

	private void checkPositioned() {
		if (!isPositioned) {
			throw new IllegalStateException("This damned was not placed in the maze!");
		}
	}
	
	public boolean isPositioned() {
		return this.isPositioned;
	}
}
