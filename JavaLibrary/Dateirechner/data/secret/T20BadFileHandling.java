import static org.junit.Assert.assertThat;
import static test.TestedProgram.main;
import static test.matchers.CLIMatchers.prints;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

import test.Input;
public class T20BadFileHandling {

	private static final String FILE_NOT_FOUND_MESSAGE = "file not found.";
	private static final String FILE_NOT_READABLE_MESSAGE = "file not readable.";

	@Test
	public void notFound() {
		assertThat(main(Input.getFreeFilename()), prints(FILE_NOT_FOUND_MESSAGE));
	}

	@Test
	public void directory() throws IOException {
		final String dirName = Input.getFreeFilename();
		final Path dir = Paths.get(dirName);
		Files.createDirectories(dir);
		dir.toFile().deleteOnExit();
		assertThat(main(dirName), prints(FILE_NOT_READABLE_MESSAGE));
	}
}
