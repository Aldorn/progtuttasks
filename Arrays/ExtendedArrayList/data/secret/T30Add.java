import test.TestObject;
import test.TestedClass;
import test.TestedProgram;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static test.matchers.UsedClassesMatcher.onlyUses;

import static test.Clazz.cs;

import org.junit.AfterClass;
import org.junit.Test;

public class T30Add {
	private final TestedClass TutorialArrayList = TestedProgram.getClass("edu.kit.informatik.util.TutorialArrayList");
	
	@Test
	public void add10() {
		TestObject list = TutorialArrayList.nev();
		for (int i = 0; i < 10; i++) {
			list.runVoid("add", cs(Object.class), "item" + i);
			assertThat(list.run(int.class, "getReservedSize"), is(10));
		}
	}
	
	@Test
	public void add5() {
		TestObject list = TutorialArrayList.nev(5);
		for (int i = 0; i < 5; i++) {
			list.runVoid("add", cs(Object.class), "item" + i);
			assertThat(list.run(int.class, "getReservedSize"), is(5));
		}
	}
	
	@Test
	public void add100() {
		TestObject list = TutorialArrayList.nev();
		for (int i = 0; i < 100; i++) {
			list.runVoid("add", cs(Object.class), "item" + i);
			if (i >= 80) {
				assertThat(list.run(int.class, "getReservedSize"), is(160));
			} else if (i >= 40) {
				assertThat(list.run(int.class, "getReservedSize"), is(80));
			} else if (i >= 20) {
				assertThat(list.run(int.class, "getReservedSize"), is(40));
			} else if (i >= 10) {
				assertThat(list.run(int.class, "getReservedSize"), is(20));
			}
		}
	}
	
	@Test
	public void add100From1() {
		TestObject list = TutorialArrayList.nev(1);
		int potence = 0;
		for (int i = 0; i < 100; i++) {
			list.runVoid("add", cs(Object.class), "item" + i);
			if (i >= Math.pow(2, potence)) {
				potence++;
			}
			assertThat(list.run(int.class, "getReservedSize"), is((int) Math.pow(2, potence)));
		}
	}
	
	@AfterClass
	public static void checkUsedPackages() {
		assertThat(TestedProgram.code, onlyUses("java.lang"));
	}
}
