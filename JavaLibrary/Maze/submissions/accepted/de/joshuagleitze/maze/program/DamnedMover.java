package de.joshuagleitze.maze.program;

import java.util.Scanner;
import java.util.regex.Pattern;

import de.joshuagleitze.maze.Coordinate;
import de.joshuagleitze.maze.Maze;

public class DamnedMover {

	private final Scanner definitionScanner;
	private static final String DELIMITER = "(?![\\d\\-])|(?<![\\d\\-])";
	private final Maze maze;
	private Damned damned;

	public DamnedMover(String definitionLine, Maze maze) {
		definitionScanner = new Scanner(definitionLine);
		definitionScanner.useDelimiter(DELIMITER);
		this.maze = maze;
	}

	public void move() throws SyntaxException {
		int x;
		int y;
		assertNext("(");
		if (!definitionScanner.hasNextInt()) {
			throw new SyntaxException("an integer", this.definitionScanner);
		}
		x = definitionScanner.nextInt();
		assertNext(",");
		if (!definitionScanner.hasNextInt()) {
			throw new SyntaxException("an integer", this.definitionScanner);
		}
		y = definitionScanner.nextInt();
		assertNext(")");
		this.damned = new Damned();
		this.damned.startOn(maze.getCellAt(new Coordinate(x, y)));
		if (!this.damned.isPositioned()) {
			return;
		}
		while (this.definitionScanner.hasNext()) {
			String symbol = this.definitionScanner.next();
			if (symbol.length() != 1 || !DirectionChar.isDefinitionChar(symbol.charAt(0))) {
				throw new SyntaxException("a direction character", symbol, this.definitionScanner);
			}
			this.damned.move(DirectionChar.fromChar(symbol.charAt(0)).getDefinedDirection());
		}
	}

	private boolean assertNext(String next) throws SyntaxException {
		if (!definitionScanner.hasNext(Pattern.quote(next))) {
			throw new SyntaxException("'" + next + "'", this.definitionScanner);
		}
		definitionScanner.next();
		return true;
	}

	public Damned getDamned() {
		return this.damned;
	}
}
