package de.joshuagleitze.maze.program;

import de.joshuagleitze.maze.Direction;

public class BadWallDefinitionException extends SyntaxException {

	private static final long serialVersionUID = 7041078173844457899L;
	final Direction badDirection;

	public BadWallDefinitionException(Direction badDirection) {
		this.badDirection = badDirection;
	}

	@Override
	protected String expectation() {
		return String.format("the wall definition does not fit to its %s one",
				this.badDirection == Direction.NORTH ? "upper" : "left");
	}
}
