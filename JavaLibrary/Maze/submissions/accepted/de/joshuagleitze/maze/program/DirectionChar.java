package de.joshuagleitze.maze.program;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import de.joshuagleitze.maze.Direction;
import static de.joshuagleitze.maze.Direction.*;

public enum DirectionChar {
	UP('↑', NORTH),
	DOWN('↓', SOUTH),
	LEFT('←', WEST),
	RIGTH('→', EAST);

	private final char character;
	private final Direction direction;
	private static final Map<Character, DirectionChar> CHAR_MAP = charMap();

	private DirectionChar(char character, Direction direction) {
		this.character = character;
		this.direction = direction;
	}

	private static Map<Character, DirectionChar> charMap() {
		DirectionChar[] values = values();
		final Map<Character, DirectionChar> map = new HashMap<>(4, 1);
		for (DirectionChar constant : values) {
			map.put(constant.character, constant);
		}
		return map;
	}

	public Direction getDefinedDirection() {
		return this.direction;
	}

	public static boolean isDefinitionChar(char character) {
		return CHAR_MAP.containsKey(character);
	}

	public static DirectionChar fromChar(char character) {
		return Optional.ofNullable(CHAR_MAP.get(character)).orElseThrow(
				() -> new IllegalArgumentException(character + "is not a direction definition character."));
	}
}
