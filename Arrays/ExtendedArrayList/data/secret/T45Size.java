import test.TestObject;
import test.TestedClass;
import test.TestedProgram;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static test.matchers.UsedClassesMatcher.*;

import static test.Clazz.cs;

import org.junit.AfterClass;
import org.junit.Test;

public class T45Size {
	private final TestedClass TutorialArrayList = TestedProgram.getClass("edu.kit.informatik.util.TutorialArrayList");

	@Test
	public void size20() {
		TestObject list = TutorialArrayList.nev();
		for (int i = 0; i < 20; i++) {
			assertThat(list.run(int.class, "getSize"), is(i));
			list.runVoid("add", cs(Object.class), "item" + i);
		}
	}
	
	@AfterClass
	public static void checkUsedPackages() {
		assertThat(TestedProgram.code, onlyUses("java.lang"));
	}
}
