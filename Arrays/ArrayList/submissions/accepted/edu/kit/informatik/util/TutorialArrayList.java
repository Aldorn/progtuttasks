package edu.kit.informatik.util;

public class TutorialArrayList {
	
	private Object[] arraylist;
	private int firstFreeIndex;
	
	public TutorialArrayList() {
		arraylist = new Object[10];
	}

	public TutorialArrayList(int size) {
		arraylist = new Object[size];
	}
	
	public void add(Object item) {
		if (firstFreeIndex >= arraylist.length) {
			enlarge();
		}
		arraylist[firstFreeIndex++] = item;
	}
	
	public Object getAt(int index) {
		if (index < 0 || index >= firstFreeIndex) {
			return null;
		}
		return arraylist[index];
	}
	
	public int getReservedSize() {
		return arraylist.length;
	}
	
	private void enlarge() {
		Object[] newList = new Object[arraylist.length * 2];
		for (int i = 0; i < arraylist.length; i++) {
			newList[i] = arraylist[i];
		}
		arraylist = newList;
	}
}
