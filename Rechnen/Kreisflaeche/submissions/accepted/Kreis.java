class Kreis {
    public static double PI = 3.14;
    public static void main(String args[]) {
        double r = Double.parseDouble(args[0]);
        System.out.println(PI * r * r);
    }
}
