import static org.junit.Assert.assertThat;
import static test.TestedProgram.main;
import static test.matchers.CLIMatchers.prints;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.junit.Test;

import test.Input;

public class T40HighNumbers {
	@Test
	public void highNumbers() {
		String mazeDef = Input.getFile("");
		int max = (1 << 15);
		final String routeDef = Input.getFile(
			//@formatter:off
			coord(max,max) + "←←←↑↑",
			coord(-max,max) + "→→→→↑↑",
			coord(max,-max) + "←←←↓↓↓↓↓",
			coord(-max,-max) + "→→→→↓↓↓↓↓"
			//@formatter:on
		);

		final String[] output = {
			//@formatter:off
			coord(max - 3, max - 2),
			coord(-max + 4, max - 2),
			coord(max - 3, -max + 5),
			coord(-max + 4, -max + 5)
			//@formatter:on
		};
		assertThat(main(mazeDef, routeDef), prints(output));
	}

	@Test
	public void highNumbersStillKeptTrackOf() {
		String mazeDef = Input.getFile("");
		int max = (1 << 14);
		final String routeDef = Input.getFile(
			//@formatter:off
			coord(max,max) + "←←←↑↑",
			coord(max,max) + "←←←↑↑",
			coord(max,max) + "←←←↑↑",
			coord(max- 3, max - 2) + "←←←↑↑"
			//@formatter:on
		);

		final String[] output = {
			//@formatter:off
			coord(max - 3, max - 2),
			coord(max - 3, max - 1),
			coord(max - 3, max),
			"lucky"
			//@formatter:on
		};
		assertThat(main(mazeDef, routeDef), prints(output));
	}

	@Test
	public void bigMazeInputFile() throws IOException {
		int max = (1 << 19);
		String mazeDef = bigMazeDef(max);
		final String routeDef = Input.getFile("(0,0)");
		assertThat(main(mazeDef, routeDef), prints("(0,0)"));
	}

	@Test
	public void bothInputFilesBig() throws IOException {
		int max = (1 << 14);
		String mazeDef = bigMazeDef(max);
		String routeDef = Input.getFreeFilename();
		File routeDefFile = new File(routeDef);
		routeDefFile.deleteOnExit();
		try (Writer routeDefWriter = new BufferedWriter(new FileWriter(routeDefFile))) {
			routeDefWriter.write("(0,0)");
			for (int i = 0; i <= max; i++) {
				routeDefWriter.write('→');
				if (i % 31 == 0) {
					routeDefWriter.write("↑↑↑");
				}
			}
			routeDefWriter.write('↑');
			for (int i = 0; i <= max; i++) {
				routeDefWriter.write('←');
				if (i % 7 == 0) {
					routeDefWriter.write("↓↓↓↓↓↓");
				}
			}
		}
		assertThat(main(mazeDef, routeDef), prints("(0,-1)"));
	}

	private String bigMazeDef(int max) throws IOException {
		String mazeDef = Input.getFreeFilename();
		File mazeDefFile = new File(mazeDef);
		mazeDefFile.deleteOnExit();
		try (Writer mazeDefWriter = new BufferedWriter(new FileWriter(mazeDefFile))) {
			mazeDefWriter.write('╶');
			for (int i = 0; i < max; i++) {
				mazeDefWriter.write('─');
			}
			mazeDefWriter.write('╴');
		}
		return mazeDef;
	}

	static String coord(int x, int y) {
		return String.format("(%d,%d)", x, y);
	}
}
