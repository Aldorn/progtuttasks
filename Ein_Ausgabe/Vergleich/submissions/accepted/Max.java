class Main {
    public static void main(String... args) {
        double a = Double.parseDouble(args[0]);
        double b = Double.parseDouble(args[1]);
        double c = Double.parseDouble(args[2]);
        double d = Double.parseDouble(args[3]);
        
        System.out.println(Math.abs(a / b - c / d) < 1e-6);
    }
}
