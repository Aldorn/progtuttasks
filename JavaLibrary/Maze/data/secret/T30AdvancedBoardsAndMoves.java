import static org.junit.Assert.assertThat;
import static test.TestedProgram.main;
import static test.matchers.CLIMatchers.prints;

import org.junit.Test;

import test.Input;

public class T30AdvancedBoardsAndMoves {

	@Test
	public void noMoves() {
		final String mazeDef = Input.getFile(
			//@formatter:off
			"          ",
			"   ┌┐  ",
			"   └┘    ",
			"          "
			//@formatter:on
		);

		final String routeDef = Input.getFile(
			//@formatter:off
			"(3,1)",
			"(3,2)"
			//@formatter:on
		);

		final String[] output = {
			//@formatter:off
			"(3,1)",
			"(3,2)"
			//@formatter:on
		};
		assertThat(main(mazeDef, routeDef), prints(output));
	}

	@Test
	public void noMovesAndLucky() {
		final String mazeDef = Input.getFile(
			//@formatter:off
			"          ",
			"   ┌┐  ",
			"   └┘    ",
			"          "
			//@formatter:on
		);

		final String routeDef = Input.getFile(
			//@formatter:off
			"(3,1)",
			"(3,1)",
			"(3,2)",
			"(3,2)"
			//@formatter:on
		);

		final String[] output = {
			//@formatter:off
			"(3,1)",
			"lucky",
			"(3,2)",
			"lucky"
			//@formatter:on
		};
		assertThat(main(mazeDef, routeDef), prints(output));
	}

	@Test
	public void differnentLineLengths() {
		final String mazeDef = Input.getFile(
			//@formatter:off
			"   ┌─┐  ",
			" ┌─┘ └─────┐ ",
			"╷│      ╶──┤",
			"│└──╴      └──╴", 
			"└───────╴",
			"  ╶──────────────────────╴  ",
			"  ╶────────╴  "
			//@formatter:on
		);

		final String routeDef = Input.getFile(
			//@formatter:off
			"(0,0)→→→→→→→→→→→→→↑→→→→→→→→→→→→→→→→→→→→→→↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓→↓↓↓↓↓↓↓↓↓↓↓",
			"(1,4)→→→→→→→→→→→→→→→→→→→→→→→→↓←←←←←←←←←←←←←←←←←←←←←←←←↑"
			//@formatter:on
		);

		final String[] output = {
			//@formatter:off
			"(25,15)",
			"(1,4)"
			//@formatter:on
		};
		assertThat(main(mazeDef, routeDef), prints(output));
	}

	@Test
	public void damnedLockIn() {
		final String mazeDef = Input.getFile(
			//@formatter:off
			"          ",
			"   ┌┐  ",
			"   └┘    ",
			"          "
			//@formatter:on
		);

		final String routeDef = Input.getFile(
			//@formatter:off
			"(3,1)↑↑→→→→→→→→→↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓←←←←",
			"(20,20)↑→↓←",
			"(21,20)↑↓",
			"(22,20)↑→↓←",
			"(20,21)←→",
			"(22,21)←→",
			"(20,22)↓←↑→",
			"(21,22)↑↓",
			"(22,22)↓→↑←",
			"(21,21)↑↑→→→→→→→→→↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓←←←←"
			//@formatter:on
		);

		final String[] output = {
			//@formatter:off
			"(3,1)",
			"(20,20)",
			"(21,20)",
			"(22,20)",
			"(20,21)",
			"(22,21)",
			"(20,22)",
			"(21,22)",
			"(22,22)",
			"(21,21)"
			//@formatter:on
		};
		assertThat(main(mazeDef, routeDef), prints(output));
	}

	@Test
	public void damnedLockInAndLucky() {
		final String mazeDef = Input.getFile(
			//@formatter:off
			"          ",
			"   ┌┐  ",
			"   └┘    ",
			"          "
			//@formatter:on
		);

		final String routeDef = Input.getFile(
			//@formatter:off
			"(3,1)↑↑→→→→→→→→→↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓←←←←",
			"(20,20)↑→↓←",
			"(21,20)↑↓",
			"(22,20)↑→↓←",
			"(20,21)←→",
			"(22,21)←→",
			"(20,22)↓←↑→",
			"(21,22)↑↓",
			"(22,22)↓→↑←",
			"(21,21)↑↑→→→→→→→→→↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓←←←←",

			"(3,1)↑↑→→→→→→→→→↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓←←←←",
			"(20,20)↑→↓←",
			"(21,20)↑↓",
			"(22,20)↑→↓←",
			"(20,21)←→",
			"(22,21)←→",
			"(20,22)↓←↑→",
			"(21,22)↑↓",
			"(22,22)↓→↑←",
			"(21,21)↑↑→→→→→→→→→↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓←←←←"
			//@formatter:on
		);

		final String[] output = {
			//@formatter:off
			"(3,1)",
			"(20,20)",
			"(21,20)",
			"(22,20)",
			"(20,21)",
			"(22,21)",
			"(20,22)",
			"(21,22)",
			"(22,22)",
			"(21,21)",
			
			"lucky",
			"lucky",
			"lucky",
			"lucky",
			"lucky",
			"lucky",
			"lucky",
			"lucky",
			"lucky",
			"lucky"
			//@formatter:on
		};
		assertThat(main(mazeDef, routeDef), prints(output));
	}

	@Test
	public void luckyDoesNotMove() {
		String mazeDef = Input.getFile("");
		final String routeDef = Input.getFile(
			//@formatter:off
			"(0,0)↑↓",
			"(0,0)↑",
			"(0,-5)↓↓↓↓↓↓↓",
			"(0,-1)↑↑↑↑↑↓↓",
			"(0,2)",
			"(0,1)",
			"(0,-2)↑↓",
			"(0,-3)↑↓",
			"(0,-4)↑↓",
			"(0,-5)↑↓",
			"(0,-6)↑↓",
			"(0,-7)↑↓"
			
			//@formatter:on
		);

		final String[] output = {
			//@formatter:off
			"(0,0)",
			"lucky",
			"(0,-1)",
			"lucky",
			"(0,2)",
			"(0,1)",
			"(0,-2)",
			"(0,-3)",
			"(0,-4)",
			"(0,-5)",
			"(0,-6)",
			"(0,-7)"
			
			//@formatter:on
		};
		assertThat(main(mazeDef, routeDef), prints(output));
	}
}
