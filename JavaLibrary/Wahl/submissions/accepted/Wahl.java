import java.util.*;
import java.util.Map.Entry;
class Wahl {
    
    public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Map<String, Integer> candidates = new HashMap<>();
		while (sc.hasNextLine()) {
			String name = sc.nextLine();
			if (!candidates.containsKey(name)) {
				candidates.put(name, 0);
			}
			candidates.put(name, candidates.get(name) + 1);
		}
		sc.close();
		ArrayList<Entry<String, Integer>> ballots = new ArrayList<>(candidates.entrySet());
		ballots.sort((x, y) -> -Integer.compare(x.getValue(), y.getValue()));
		ArrayList<String> winners = new ArrayList<>();
		for (Entry<String, Integer> e: ballots) {
			if (e.getValue() == ballots.get(0).getValue()) {
				winners.add(e.getKey());
			}
		}
		if (winners.size() == 1) {
			System.out.println("Informatiker des Jahres: " + winners.get(0));
		} else {
			System.out.println("Unentschieden");
		}
    }
    
}
