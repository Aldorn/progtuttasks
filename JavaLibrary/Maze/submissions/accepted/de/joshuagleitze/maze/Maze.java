package de.joshuagleitze.maze;

import java.util.HashMap;
import java.util.Map;

public class Maze {

	/*
	 * Implementation note regarding the data structure:
	 * 
	 * There are two intuitive possibilities to model the maze:
	 * 
	 * a) Something like List<List<MazeCell>>. If using this, choosing the list
	 * implementation is difficult: In an ArrayList, the copy operations are
	 * very to costly if there are high coordinates in the route definition.
	 * However, should the route definition be very big, the lookup costs of the
	 * LinkedList are very to high.
	 * 
	 * b) A Map<Coordinate, MazeCell>. If choosing a HashMap, the performance
	 * will get worse with more cells being inserted into the map. *But*: While
	 * coordinates can get big really easy (just enter high numbers in the route
	 * definition), there will usually be a relatively small number of cells
	 * that are actually needed (because the input files can only be so big).
	 * 
	 * While both approaches will pass the DOMJudge tests, using a HashMap fits
	 * the expected use cases better. It performs very well for both high
	 * coordinates and big input files.
	 */
	private final Map<Coordinate, MazeCell> cells = new HashMap<>();

	public MazeCell getCellAt(Coordinate coordinate) {
		return cells.computeIfAbsent(coordinate, coord -> new MazeCell(coord, this));
	}
}

//public class Maze {
//
//	int zeroShift = 0;
//	private final List<MazeCellLine> board = new LinkedList<>();
//
//	public MazeCell getCellAt(Coordinate coordinate) {
//		int shifted = coordinate.getY() + zeroShift;
//		while (shifted >= board.size()) {
//			board.add(new MazeCellLine(board.size() - zeroShift));
//		}
//		while (shifted < 0) {
//			board.add(0, new MazeCellLine(-(++zeroShift)));
//			shifted = coordinate.getY() + zeroShift;
//		}
//		MazeCell cell = board.get(shifted).getCellAt(coordinate.getX());
//		assert cell.getPosition().equals(coordinate);
//		return cell;
//	}
//
//	private class MazeCellLine {
//		private List<MazeCell> cells = new LinkedList<>();
//		private int zeroShift = 0;
//		private int y;
//
//		private MazeCellLine(int y) {
//			this.y = y;
//		}
//
//		MazeCell getCellAt(int index) {
//			int shifted = index + zeroShift;
//			while (shifted >= cells.size()) {
//				cells.add(new MazeCell(new Coordinate(cells.size() - zeroShift, y), Maze.this));
//			}
//			while (shifted < 0) {
//				cells.add(0, new MazeCell(new Coordinate(-(++zeroShift), y), Maze.this));
//				shifted = index + zeroShift;
//			}
//			return cells.get(shifted);
//		}
//	}
//}
