import static org.junit.Assert.assertThat;
import static test.matchers.UsedClassesMatcher.onlyUses;

import org.junit.AfterClass;
import org.junit.Test;

import test.TestedClass;
import test.TestedProgram;

public class T10Constructor {
	private final TestedClass TutorialArrayList = TestedProgram.getClass("edu.kit.informatik.MySet");

	@Test
	public void parameterlessConstructor() {
		TutorialArrayList.nev();
	}
		
}
