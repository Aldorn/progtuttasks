
import static org.junit.Assert.assertThat;
import static test.TestedProgram.main;
import static test.matchers.CLIMatchers.prints;

import org.junit.Test;

import test.Input;

public class T10CorrectResult {

	@Test
	public void noCalculation() {
		final String input = Input.getFile("");
		assertThat("If no calculations are done, the result is 1", main(input), prints("1"));
	}

	@Test
	public void simpleMultiplication() {
		final String input = Input.getFile("* 1");
		assertThat("'* 1' results in 1", main(input), prints("1"));
	}

	@Test
	public void simpleAddition() {
		final String input = Input.getFile("+ 5");
		assertThat("'+ 5' results in 6", main(input), prints("6"));
	}

	@Test
	public void simpleCalculation() {
		final String input = Input.getFile("+ 5 9 2", "* 2 3", "+ 6");
		assertThat(main(input), prints("108"));
	}

	@Test
	public void longCalculation() {
		final String input = Input.getFile("+ 1", "* 2", "* 2", "* 2 2 2 2 2", "+ 4 40", "* 3");
		assertThat(main(input), prints("900"));
	}
}
