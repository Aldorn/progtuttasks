package de.joshuagleitze.util;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * Simple class that only calculates a value if it’s needed.
 *
 * @author Joshua Gleitze
 *
 * @param <TYPE>
 *            The type of the calculated value.
 */
public class Lazy<TYPE> implements Supplier<TYPE> {

	private TYPE value;
	private Supplier<TYPE> supplier;

	/**
	 * Creates a new cache. On first request to {@link #get}, {@code supplier}
	 * will be used to generate the value. The created value will be returned
	 * afterwards.
	 *
	 * @param supplier
	 *            The value supplier.
	 */
	public Lazy(final Supplier<TYPE> supplier) {
		this.supplier = supplier;
	}

	@Override
	public TYPE get() {
		if (this.supplier != null) {
			this.value = this.supplier.get();
			this.supplier = null;
		}
		return this.value;
	}
	
	@Override
	public String toString() {
		return "Lazy[" + (this.supplier == null ? Objects.toString(value) : "") + "]";
	}
}
