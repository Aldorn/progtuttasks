import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static test.Clazz.cs;
import static test.matchers.UsedClassesMatcher.onlyUses;

import org.junit.AfterClass;
import org.junit.Test;

import test.TestObject;
import test.TestedClass;
import test.TestedProgram;

public class T80Mixed {
	private final TestedClass TutorialArrayList = TestedProgram.getClass("edu.kit.informatik.util.TutorialArrayList");

	@Test
	public void bigScenario() {
		TestObject list = TutorialArrayList.nev(4);
		for (int i = 0; i < 64; i++) {
			list.runVoid("add", cs(Object.class), "item");
		}
		assertThat(list.run(int.class, "getSize"), is(64));
		assertThat(list.run(int.class, "getReservedSize"), is(64));
		list.runVoid("removeAt", 20);
		list.runVoid("insertAt", cs(Object.class, int.class), "otherItem", 20);
		assertThat(list.run(int.class, "getSize"), is(64));
		assertThat(list.run(int.class, "getReservedSize"), is(64));
		list.runVoid("insertAt", cs(Object.class, int.class), "otherItem", 20);
		assertThat(list.run(Object.class, "getAt", 19), is("item"));
		assertThat(list.run(Object.class, "getAt", 20), is("otherItem"));
		assertThat(list.run(Object.class, "getAt", 21), is("otherItem"));
		assertThat(list.run(Object.class, "getAt", 22), is("item"));
		assertThat(list.run(int.class, "getSize"), is(65));
		assertThat(list.run(int.class, "getReservedSize"), is(128));
		assertThat(list.run(boolean.class, "remove", cs(Object.class), (Object) null), is(false));
		assertThat(list.run(int.class, "getSize"), is(65));
		assertThat(list.run(int.class, "getReservedSize"), is(128));
		assertThat(list.run(boolean.class, "remove", cs(Object.class), "item"), is(true));
		assertThat(list.run(int.class, "getSize"), is(2));
		assertThat(list.run(int.class, "getReservedSize"), is(4));
		assertThat(list.run(boolean.class, "remove", cs(Object.class), "item"), is(false));
		assertThat(list.run(Object.class, "getAt", -1), is((Object) null));
		assertThat(list.run(Object.class, "getAt", 0), is("otherItem"));
		assertThat(list.run(Object.class, "getAt", 1), is("otherItem"));
		assertThat(list.run(Object.class, "getAt", 2), is((Object) null));
		assertThat(list.run(int.class, "getSize"), is(2));
		assertThat(list.run(int.class, "getReservedSize"), is(4));
		for (int i = 0; i < 126; i++) {
			list.runVoid("add", cs(Object.class), "item");
		}
		assertThat(list.run(int.class, "getSize"), is(128));
		assertThat(list.run(int.class, "getReservedSize"), is(128));
		for (int i = 0; i < 128; i += 2) {
			list.runVoid("removeAt", i);
			assertThat(list.run(Object.class, "getAt", 127), is((Object) null));
			list.runVoid("insertAt", cs(Object.class, int.class), "yetAnotherItem", i);
		}
		assertThat(list.run(int.class, "getSize"), is(128));
		assertThat(list.run(int.class, "getReservedSize"), is(128));
		assertThat(list.run(Object.class, "getAt", 0), is("yetAnotherItem"));
		assertThat(list.run(Object.class, "getAt", 1), is("otherItem"));
		for (int i = 2; i < 128; i++) {
			if (i % 2 == 0) {
				assertThat(list.run(Object.class, "getAt", i), is("yetAnotherItem"));
			} else {
				assertThat(list.run(Object.class, "getAt", i), is("item"));
			}
		}
		for (int i = 0; i < 128; i += 2) {
			list.runVoid("removeAt", i);
			assertThat(list.run(Object.class, "getAt", 127), is((Object) null));
			list.runVoid("insertAt", cs(Object.class, int.class), null, i);
		}
		assertThat(list.run(int.class, "getSize"), is(128));
		assertThat(list.run(int.class, "getReservedSize"), is(128));
		for (int i = 2; i < 128; i++) {
			if (i % 2 == 0) {
				assertThat(list.run(Object.class, "getAt", i), is((Object) null));
			} else {
				assertThat(list.run(Object.class, "getAt", i), is("item"));
			}
		}
		assertThat(list.run(boolean.class, "remove", cs(Object.class), (Object) null), is(true));
		assertThat(list.run(int.class, "getSize"), is(64));
		assertThat(list.run(int.class, "getReservedSize"), is(128));
		assertThat(list.run(boolean.class, "remove", cs(Object.class), (Object) null), is(false));
		assertThat(list.run(int.class, "getSize"), is(64));
		assertThat(list.run(int.class, "getReservedSize"), is(128));
		assertThat(list.run(Object.class, "getAt", 64), is((Object) null));
		assertThat(list.run(Object.class, "getAt", 65), is((Object) null));
		assertThat(list.run(Object.class, "getAt", 66), is((Object) null));
	}
	
	@AfterClass
	public static void checkUsedPackages() {
		assertThat(TestedProgram.code, onlyUses("java.lang"));
	}
}
