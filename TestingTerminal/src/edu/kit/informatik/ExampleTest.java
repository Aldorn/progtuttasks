package edu.kit.informatik;
import static org.hamcrest.Matchers.*; //see: http://hamcrest.org/JavaHamcrest/javadoc/1.3/org/hamcrest/Matchers.html


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Example to demonstrate the use of the modified Terminal class.
 * 
 * @author moritz
 *
 */
public class ExampleTest {

    @Before
    public void init() {
        Terminal.reset(); // Important to tell the Terminal not to check output on the first input
    }

    @After
    public void cleanUp() {
        Terminal.flush(); // Important to tell the Terminal to check the last output
    }

    @Test
    public void test1() {

        // initialize Terminal

        // add student should print out OK
        Terminal.addSingleLineOutputThatIsExactly("add student", "OK");
        // list students can output students in any order
        Terminal.addMultipleLineOutputThatMatches("list students", containsInAnyOrder("Emil", "Paul"));
        // quit should not print out anything
        Terminal.addNoOutput("quit");

        // starts program with no arguments
        CampusManagement.main(null);

    }

    @Test
    public void test2() {
        // command should print out a single line that has a 2 in it somewhere
        Terminal.addSingleLineOutputThatMatches("get nrofstudents", containsString("2"));
        // the output for this command should start with "Error", since "bla bla
        // bla" isn't a valid command
        Terminal.addSingleLineOutputThatMatches("bla bla bla", startsWith("Error"));
        CampusManagement.main(null);
    }

}
