#!/usr/bin/python3

import math
from random import randint as r, choice
from argparse import ArgumentParser as Parser
from pathlib import Path

label_x = 'abcdefgh'
label_y = [8, 7, 6, 5, 4, 3, 2, 1]

def genline(l, tx, ty):
    if l == ty:
        return ' '.join(['X' if i != tx else 'T' for i in range(8)])
    else:
        return ' '.join(['_#'[(i + l) % 2] if i != tx else 'X' for i in range(8)])

def create_test(i=0, n=4):
    tx = r(0, 7)
    ty = r(0, 7)
    instring = '{}{}'.format(label_x[tx], label_y[ty])
    outstring = '\n'.join([genline(i, tx, ty) for i in range(8)])
    return (instring, outstring)

def main():
    parser = Parser(description='Generate test cases')
    parser.add_argument('-n', '--number', default=10, type=int, dest='n',\
        help = 'number of test cases to generate')
    parser.add_argument('-f', '--filename', default='test', type=str, dest='f',\
        help = 'base filename used')
    parser.add_argument('-p', '--path', default='secret', type=str, dest='p',\
        help = 'path to store generated test files (default secret)')

    args = parser.parse_args()
    
    cnt = args.n
    name = args.f
    testpath = args.p
    p = Path(testpath)
    if not p.exists():
        p.mkdir(parents=True)
    print(args)
    print(cnt, name, testpath)
    
    for i in range(cnt):
        (instring, outstring) = create_test(i, cnt)
        filename = '{}/{:}_{:0>{width}}'.format(testpath, name, i, width=len(str(cnt - 1)))
        with open(filename + '.in', 'w') as infile:
            print(instring)
            infile.writelines(instring)
        with open(filename + '.ans', 'w') as outfile:
            print(outstring)
            outfile.write(outstring)

        #print(filename, p, h, d)
    pass

main()
