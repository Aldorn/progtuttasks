class Brutus {
    private int key;
    private static final int ROT = 'Z' - 'A' + 1;
    private static final int ABC_MASK = 0x40;
    public static String JAVA_STRING = "public static void main";
    
    
    Brutus(int key) {
        this.key = key;
    }
    
    private char rotateChar(char c, int k) {
        return (c >> 6) == 1 && ((char)((c & 0x1F) - 1)) < 26 ? (char)(((c & 0x1F) + k - 1 + ROT) % ROT + 1 | c & ~0x1F) : c;
    }
    
    public String encrypt(String s) {
        StringBuilder sb = new StringBuilder(s.length());
        for (int i = 0; i < s.length(); ++i) {
            sb.append(rotateChar(s.charAt(i), key));
        }
        return sb.toString();
    }
    
    public String decrypt(String s) {
        StringBuilder sb = new StringBuilder(s.length());
        for (int i = 0; i < s.length(); ++i) {
            sb.append(rotateChar(s.charAt(i), -key));
        }
        return sb.toString();
    }
    
    public static void main(String... args) {
        //Caesar c = new Caesar(args[1].toLowerCase().charAt(0) - 'a');
        String secret = args[0];
        String decrypted;
        for (int i = 0; i < ROT; ++i) {
            Brutus c = new Brutus(i);
            if ((decrypted = c.decrypt(secret)).indexOf(JAVA_STRING) != -1) {
                System.out.println((char)(i + 'a'));
                System.out.println(decrypted);
                return;
            }
        }
        System.out.println("Verrat!");
    }
}
