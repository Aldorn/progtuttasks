import java.util.Scanner;

public class MatrixMultiplication {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		// Lines & Columns
		int linesA = sc.nextInt();
		int columnsA = sc.nextInt();
		int columnsB = sc.nextInt();
		
		int[][] matrixA = readInMatrix(linesA, columnsA, sc);
		int[][] matrixB = readInMatrix(columnsA, columnsB, sc);
		
		// C = A * B
		int[][] matrixC = new int[linesA][columnsB];
		for (int i = 0; i < linesA; i++) {
			for (int j = 0; j < columnsB; j++) {
				int result = 0;
				for (int k = 0; k < columnsA; k++) {
					result += matrixA[i][k] * matrixB[k][j];
				}
				matrixC[i][j] = result;
			}
		}
		
		// Ausgabe der Matrix C
		for (int i = 0; i < linesA; i++) {
			for (int j = 0; j < columnsB; j++) {
				System.out.print(matrixC[i][j] + " ");
			}
			System.out.println("");
		}
	}
	
	private static int[][] readInMatrix(int lines, int columns, Scanner scanner) {
		int[][] matrix = new int[lines][columns];
		for (int i = 0; i < lines; i++) {
			for (int j = 0; j < columns; j++) {
				matrix[i][j] = scanner.nextInt();
			}
		}
		return matrix;
	}

}
