import static org.junit.Assert.assertThat;
import static test.TestedProgram.main;
import static test.matchers.CLIMatchers.printsAndErrorStartingWith;
import static test.matchers.CLIMatchers.printsErrorStartingWith;

import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.Test;

import test.Input;

public class T60ErrorChecking {

	private static final String[] STANDARD_ROUTE_DEFINITION = { "(0,0)↑" };
	private static final String[] STANDARD_MAZE_DEFINITION = {
		//@formatter:off
		"┌─┬────────┐",
		"│╷│┌──────╴│",
		"││││ ┌──┬╴╶┤",
		"││││ │╷╶┼╴╷│",
		"││╵│ └┘ │ ││",
		"│└─┴────┴╴╵│",
		"╵╶─────────┘"
		//@formatter:on
	};

	private static String standardRoute() {
		return Input.getFile(STANDARD_ROUTE_DEFINITION);
	}

	private static String standardMaze() {
		return Input.getFile(STANDARD_MAZE_DEFINITION);
	}

	@Test
	public void commandLineArguments() {
		assertThat(main(), printsErrorStartingWith("bad arguments"));
		assertThat(main(Input.getFile("")), printsErrorStartingWith("bad arguments"));
	}

	@Test
	public void badCharacterInMazeDefinition() {
		String mazeDef = Input.getFile(
			//@formatter:off
			"┌─┬────────┐",
			"│╷│┌──o───╴│",
			"││││ ┌──┬╴╶┤",
			"││││ │╷╶┼╴╷│",
			"││╵│ └┘ │ ││",
			"│└─┴────┴╴╵│",
			"╵╶─────────┘"
			//@formatter:on
		);
		assertThat(main(mazeDef, standardRoute()), printsErrorStartingWith(
				"syntax error in maze definition, line 2, character 7: expected a wall definition character, but found 'o'"));

		mazeDef = Input.getFile(
			//@formatter:off
			"┌─┬────────┐",
			"│╷│┌──────╴│",
			"││││ ┌──┬╴╶┤",
			"││││ │╷╶┼╴╷│",
			"││╵│ └┘ │ ││",
			"│└─┴────┴╴╵│",
			"╵╶─────────┘-"
			//@formatter:on
		);
		assertThat(main(mazeDef, standardRoute()), printsErrorStartingWith(
				"syntax error in maze definition, line 7, character 13: expected a wall definition character, but found '-'"));
	}

	@Test
	public void badCharacterInRouteDefinition() {
		String routeDef = Input.getFile(
			//@formatter:off
			"(-2,8)→↑→↑↑→→→→→→→→→↑←",
			"-1,6)↑→→→→→"
			//@formatter:on
		);
		String[] output = { "(8,4)" };
		assertThat(main(standardMaze(), routeDef), printsAndErrorStartingWith(output,
				"syntax error in route definition, line 2, character 1: expected '(', but found '-"));

		routeDef = Input.getFile(
			//@formatter:off
			"(-2,8)→↑→↑↑→→→→→→→→→↑←",
			"(,6)↑→→→→→"
			//@formatter:on
		);
		assertThat(main(standardMaze(), routeDef), printsAndErrorStartingWith(output,
				"syntax error in route definition, line 2, character 2: expected an integer, but found ',"));

		routeDef = Input.getFile(
			//@formatter:off
			"(-2,8)→↑→↑↑→→→→→→→→→↑←",
			"(f,6)↑→→→→→"
			//@formatter:on
		);
		assertThat(main(standardMaze(), routeDef), printsAndErrorStartingWith(output,
				"syntax error in route definition, line 2, character 2: expected an integer, but found 'f"));

		routeDef = Input.getFile(
			//@formatter:off
			"(-2,8)→↑→↑↑→→→→→→→→→↑←",
			"(-1|6)↑→→→→→"
			//@formatter:on
		);
		assertThat(main(standardMaze(), routeDef), printsAndErrorStartingWith(output,
				"syntax error in route definition, line 2, character 4: expected ',', but found '|"));

		routeDef = Input.getFile(
			//@formatter:off
			"(-2,8)→↑→↑↑→→→→→→→→→↑←",
			"(-1,)↑→→→→→"
			//@formatter:on
		);
		assertThat(main(standardMaze(), routeDef), printsAndErrorStartingWith(output,
				"syntax error in route definition, line 2, character 5: expected an integer, but found ')"));

		routeDef = Input.getFile(
			//@formatter:off
			"(-2,8)→↑→↑↑→→→→→→→→→↑←",
			"(-1,f)↑→→→→→"
			//@formatter:on
		);
		assertThat(main(standardMaze(), routeDef), printsAndErrorStartingWith(output,
				"syntax error in route definition, line 2, character 5: expected an integer, but found 'f"));

		routeDef = Input.getFile(
			//@formatter:off
			"(-2,8)→↑→↑↑→→→→→→→→→↑←",
			"(-1)↑→→→→→"
			//@formatter:on
		);
		assertThat(main(standardMaze(), routeDef), printsAndErrorStartingWith(output,
				"syntax error in route definition, line 2, character 4: expected ',', but found ')"));

		routeDef = Input.getFile(
			//@formatter:off
			"(-2,8)→↑→↑↑→→→→→→→→→↑←",
			"(-1,))↑→→→→→"
			//@formatter:on
		);
		assertThat(main(standardMaze(), routeDef), printsAndErrorStartingWith(output,
				"syntax error in route definition, line 2, character 5: expected an integer, but found ')"));

		routeDef = Input.getFile(
			//@formatter:off
			"(-2,8)→↑→↑↑→→→→→→→→→↑←",
			"(-1,8↑→→→→→"
			//@formatter:on
		);
		assertThat(main(standardMaze(), routeDef), printsAndErrorStartingWith(output,
				"syntax error in route definition, line 2, character 6: expected ')', but found '↑"));

		routeDef = Input.getFile(
			//@formatter:off
			"(-2,8)→↑→↑↑→→→→→→→→→↑←",
			"(-1,8))↑→→→→→"
			//@formatter:on
		);
		assertThat(main(standardMaze(), routeDef), printsAndErrorStartingWith(output,
				"syntax error in route definition, line 2, character 7: expected a direction character, but found ')"));

		routeDef = Input.getFile(
			//@formatter:off
			"(-2,8)→↑→↑↑→→→→→→→→→↑←",
			"(-1,8)↑→→9→→"
			//@formatter:on
		);
		assertThat(main(standardMaze(), routeDef), printsAndErrorStartingWith(output,
				"syntax error in route definition, line 2, character 10: expected a direction character, but found '9"));
	}

	private char[][] wallCheckBoard() {
		return Stream.of(
			//@formatter:off
			"┌┬╴╶┐",
			"├┼──┤",
			"╵│  │",
			"╷│  │",
			"└┴──┘"
			//@formatter:on
		).map(String::toCharArray).toArray(char[][]::new);
	}

	@Test
	public void badWallDefinition() {
		WallDefinition[] defs = WallDefinition.values();
		final char[][] checkBoard = wallCheckBoard();
		for (int i = 0; i < checkBoard.length; i++) {
			for (int j = 0; j < checkBoard[i].length; j++) {
				char oldchar = checkBoard[i][j];
				for (WallDefinition wDef : defs) {
					if (oldchar == wDef.toChar()) {
						continue;
					}
					WallDefinition upDef = readDef(checkBoard, i - 1, j);
					checkBoard[i][j] = wDef.toChar();
					if (hasWall(upDef, Direction.SOUTH) != hasWall(wDef, Direction.NORTH)) {
						badWallCharTest(checkBoard, i, j, true);
						continue;
					}
					WallDefinition leftDef = readDef(checkBoard, i, j - 1);
					if (hasWall(leftDef, Direction.EAST) != hasWall(wDef, Direction.WEST)) {
						badWallCharTest(checkBoard, i, j, false);
						continue;
					}
					WallDefinition rightDef = readDef(checkBoard, i, j + 1);
					if (hasWall(rightDef, Direction.WEST) != hasWall(wDef, Direction.EAST)) {
						badWallCharTest(checkBoard, i, j + 1, false);
						continue;
					}
					badWallCharTest(checkBoard, i + 1, j, true);
				}
				checkBoard[i][j] = oldchar;
			}
		}
	}

	@Test
	public void badBottomRowWallDefinition() {
		char[][] checkBoard = wallCheckBoard();
		checkBoard[4][0] = WallDefinition.RIGHTTOPBOTTOM.toChar();
		badWallCharTest(checkBoard, 5, 0, true);
		checkBoard = wallCheckBoard();
		checkBoard[4][1] = WallDefinition.ALL.toChar();
		badWallCharTest(checkBoard, 5, 1, true);
		checkBoard = wallCheckBoard();
		checkBoard[4][2] = WallDefinition.LEFTRIGHTBOTTOM.toChar();
		badWallCharTest(checkBoard, 5, 2, true);
		checkBoard = wallCheckBoard();
		checkBoard[4][3] = WallDefinition.LEFTRIGHTBOTTOM.toChar();
		badWallCharTest(checkBoard, 5, 3, true);
		checkBoard = wallCheckBoard();
		checkBoard[4][4] = WallDefinition.LEFTTOPBOTTOM.toChar();
		badWallCharTest(checkBoard, 5, 4, true);
	}

	private static char[][] complicatedWallCheckBoard() {
		return Stream.of(
			//@formatter:off
			"   ┌─┐  ",
			" ┌─┘ └─────┐",
			"╷│      ╶──┤",
			"│└──────┐  └──╴", 
			"└───────┤",
			"  ╶─────┴──┬─────────────╴",
			"  ╶────────┘"
			//@formatter:on
		).map(String::toCharArray).toArray(char[][]::new);
	}
	@Test
	public void badRightColumnWallDefinition() {
		char[][] checkBoard = complicatedWallCheckBoard();
		checkBoard[0][7] = WallDefinition.RIGHT.toChar();
		badWallCharTest(checkBoard, 0, 8, false);
		checkBoard = complicatedWallCheckBoard();
		checkBoard[1][11] = WallDefinition.LEFTRIGHTBOTTOM.toChar();
		badWallCharTest(checkBoard, 1, 12, false);
		checkBoard = complicatedWallCheckBoard();
		checkBoard[2][11] = WallDefinition.ALL.toChar();
		badWallCharTest(checkBoard, 2, 12, false);
		checkBoard = complicatedWallCheckBoard();
		checkBoard[3][14] = WallDefinition.LEFTRIGHT.toChar();
		badWallCharTest(checkBoard, 3, 15, false);
		checkBoard = complicatedWallCheckBoard();
		checkBoard[4][8] = WallDefinition.ALL.toChar();
		badWallCharTest(checkBoard, 4, 9, false);
		checkBoard = complicatedWallCheckBoard();
		checkBoard[5][25] = WallDefinition.LEFTRIGHT.toChar();
		badWallCharTest(checkBoard, 5, 26, false);
		checkBoard = complicatedWallCheckBoard();
		checkBoard[6][11] = WallDefinition.LEFTRIGHTTOP.toChar();
		badWallCharTest(checkBoard, 6, 12, false);
	}

	private boolean hasWall(WallDefinition wallDef, Direction dir) {
		return wallDef.getDefinedWalls().contains(dir);
	}

	private void badWallCharTest(char[][] board, int i, int j, boolean isUpper) {
		String[] maze = Arrays.stream(board).map(String::new).toArray(String[]::new);
		assertThat(main(Input.getFile(maze), standardRoute()),
				printsErrorStartingWith(String.format(
						"syntax error in maze definition, line %d, character %d: the wall definition does not fit to its %s one",
						i + 1, j + 1, isUpper ? "upper" : "left")));
	}

	private WallDefinition readDef(char[][] board, int i, int j) {
		if (i >= board.length || i < 0 || j >= board[i].length || j < 0) {
			return WallDefinition.NONE;
		}
		return WallDefinition.fromChar(board[i][j]);
	}

	private enum WallDefinition {
		NONE(' '),
		LEFT('╴', Direction.WEST),
		TOP('╵', Direction.NORTH),
		RIGHT('╶', Direction.EAST),
		BOTTOM('╷', Direction.SOUTH),
		LEFTRIGHT('─', Direction.EAST, Direction.WEST),
		TOPBOTTOM('│', Direction.NORTH, Direction.SOUTH),
		RIGHTBOTTOM('┌', Direction.EAST, Direction.SOUTH),
		LEFTBOTTOM('┐', Direction.WEST, Direction.SOUTH),
		RIGHTTOP('└', Direction.EAST, Direction.NORTH),
		LEFTTOP('┘', Direction.WEST, Direction.NORTH),
		RIGHTTOPBOTTOM('├', Direction.EAST, Direction.NORTH, Direction.SOUTH),
		LEFTTOPBOTTOM('┤', Direction.WEST, Direction.NORTH, Direction.SOUTH),
		LEFTRIGHTBOTTOM('┬', Direction.WEST, Direction.EAST, Direction.SOUTH),
		LEFTRIGHTTOP('┴', Direction.WEST, Direction.EAST, Direction.NORTH),
		ALL('┼', Direction.NORTH, Direction.EAST, Direction.SOUTH, Direction.WEST);

		private final Set<Direction> definedWalls = EnumSet.noneOf(Direction.class);
		private final char character;
		private static final Map<Character, WallDefinition> CHAR_MAP = charMap();

		private WallDefinition(char character, Direction... walls) {
			this.definedWalls.addAll(Arrays.asList(walls));
			this.character = character;
		}

		private static Map<Character, WallDefinition> charMap() {
			WallDefinition[] values = values();
			final Map<Character, WallDefinition> map = new HashMap<>(values.length, 1);
			for (WallDefinition constant : values) {
				map.put(constant.character, constant);
			}
			return map;
		}

		private Set<Direction> getDefinedWalls() {
			return Collections.unmodifiableSet(this.definedWalls);
		}

		private static WallDefinition fromChar(char character) {
			return Optional.ofNullable(CHAR_MAP.get(character))
					.orElseThrow(() -> new IllegalArgumentException(character + "is not a wall definition character."));
		}

		private char toChar() {
			return this.character;
		}
	}

	private enum Direction {
		NORTH,
		EAST,
		SOUTH,
		WEST;
	}
}
