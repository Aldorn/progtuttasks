package de.joshuagleitze.maze;

public enum Direction {
	NORTH(0, -1) {
		@Override
		public Direction getOpposite() {
			return SOUTH;
		}
	},
	EAST(1, 0) {
		@Override
		public Direction getOpposite() {
			return WEST;
		}
	},
	SOUTH(0, 1) {
		@Override
		public Direction getOpposite() {
			return NORTH;
		}
	},
	WEST(-1, 0) {
		@Override
		public Direction getOpposite() {
			return EAST;
		}
	};
	final int deltaX;
	final int deltaY;

	private Direction(int deltaX, int deltaY) {
		this.deltaX = deltaX;
		this.deltaY = deltaY;
	}

	public abstract Direction getOpposite();

	public boolean isForward() {
		return deltaX + deltaY > 0;
	};

	public Coordinate move(Coordinate position) {
		return new Coordinate(position.getX() + deltaX, position.getY() + deltaY);
	};
}
