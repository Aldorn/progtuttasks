package de.joshuagleitze.maze.program;

public class DefinitionFileNotFoundException extends MazeException {

	private static final long serialVersionUID = 2928864029951427885L;

	@Override
	public String getMessage() {
		return String.format("%s not found", this.fileName);
	}
}
