import test.Input;

import static test.TestedProgram.main;
import static test.matchers.CLIMatchers.*;

import org.junit.Test;

import static org.junit.Assert.assertThat;

public class T10CorrectBoard {

	@Test
	public void figureTest() {
		final String mazeDef = Input.getFile(
			//@formatter:off
			"┌────────┐",
			"│┌──────╴│",
			"││╶───┬──┤",
			"│└──╴╶┴─╴│", 
			"╵╶───────┘"
			//@formatter:on
		);
		final String routeDef = Input.getFile("(0,0)");
		assertThat(main(mazeDef, routeDef), prints("(0,0)"));
	}

	@Test
	public void emptyTest() {
		String mazeDef = Input.getFile("");
		final String routeDef = Input.getFile("(0,0)");
		assertThat(main(mazeDef, routeDef), prints("(0,0)"));
		mazeDef = Input.getFile("             ", "", "      ", "  ");
		assertThat(main(mazeDef, routeDef), prints("(0,0)"));
	}
}
