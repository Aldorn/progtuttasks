package de.joshuagleitze.maze.program;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Set;

import de.joshuagleitze.maze.Coordinate;
import de.joshuagleitze.maze.Direction;
import de.joshuagleitze.maze.Maze;
import de.joshuagleitze.maze.MazeCell;

public class MazeFromFileBuilder {
	boolean isBuilt = false;
	final Maze resultMaze = new Maze();
	final Path definitionFile;
	private MazeCell lineStart;
	private MazeCell momentaryCell;
	private int lastMaxX;
	private Optional<SyntaxException> parseException = Optional.empty();

	public MazeFromFileBuilder(Path definitionFile) {
		this.definitionFile = definitionFile;
	}

	public synchronized Maze build() throws MazeException {
		if (this.isBuilt) {
			if (parseException.isPresent()) {
				throw parseException.get();
			}
		} else {
			this.isBuilt = true;
			lineStart = resultMaze.getCellAt(new Coordinate(-1, -1));
			momentaryCell = lineStart;
			try (final BufferedReader lineReader = Files.newBufferedReader(definitionFile)) {
				for (String line = lineReader.readLine(); line != null; line = lineReader.readLine()) {
					this.processLine(line);
				}
				checkLineToEnd(); // virtual last line
			} catch (SyntaxException syntaxError) {
				syntaxError.setLineNumber(this.lineStart.getPosition().getY() + 2);
				syntaxError.setCharacterPosition(this.momentaryCell.getPosition().getX() + 2);
				throw syntaxError;
			} catch (NoSuchFileException | FileNotFoundException noSuchFile) {
				throw new DefinitionFileNotFoundException();
			} catch (IOException e) {
				throw new DefinitionFileNotReadableException(e);
			} 
		}
		return this.resultMaze;
	}

	private void processLine(String line) throws SyntaxException {
		for (char character : line.toCharArray()) {
			this.processChar(character);
			this.moveRight();
		}
		lastMaxX = momentaryCell.getPosition().getX();
		checkLineToEnd();
		lineStart = lineStart.getNeigbour(Direction.SOUTH);
		momentaryCell = lineStart;
	}

	private void processChar(char character) throws SyntaxException {
		if (!WallDefinition.isDefinitionChar(character)) {
			throw new SyntaxException("a wall definition character", character);
		}
		this.processWallDefinition(WallDefinition.fromChar(character));
	}
	
	private void processWallDefinition(WallDefinition wallDefinition) throws BadWallDefinitionException {
		Set<Direction> definedWalls = wallDefinition.getDefinedWalls();
		if (definedWalls.contains(Direction.NORTH) != momentaryCell.hasWallTo(Direction.EAST)) {
				throw new BadWallDefinitionException(Direction.NORTH);
		}
		if (definedWalls.contains(Direction.WEST) != momentaryCell.hasWallTo(Direction.SOUTH)) {
				throw new BadWallDefinitionException(Direction.WEST);
		}
		if (definedWalls.contains(Direction.EAST)) {
			momentaryCell.getNeigbour(Direction.EAST).setHasWallTo(Direction.SOUTH);
		}
		if (definedWalls.contains(Direction.SOUTH)) {
			momentaryCell.getNeigbour(Direction.SOUTH).setHasWallTo(Direction.EAST);
		}
	}
	
	private void checkLineToEnd() throws SyntaxException {
		for (; momentaryCell.getPosition().getX() <= lastMaxX; moveRight()) {
			this.processWallDefinition(WallDefinition.NONE);;
		}
	}

	private void moveRight() {
		this.momentaryCell = this.momentaryCell.getNeigbour(Direction.EAST);
	}

}
