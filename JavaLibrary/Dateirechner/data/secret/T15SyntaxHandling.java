import static org.junit.Assert.assertThat;
import static test.TestedProgram.main;
import static test.matchers.CLIMatchers.prints;
import static test.matchers.CLIMatchers.printsSomethingStartingWith;

import org.junit.Test;

import test.Input;

public class T15SyntaxHandling {
	private static final String BAD_SYNTAX_MESSAGE = "bad syntax.";

	@Test
	public void noOperator() {
		String input = Input.getFile("5 6");
		assertThat("no operator in a line", main(input), printsSomethingStartingWith(BAD_SYNTAX_MESSAGE));
		input = Input.getFile("+ 4 3", "* 2 5", "3 4");
		assertThat("no operator in a line", main(input), printsSomethingStartingWith(BAD_SYNTAX_MESSAGE));
	}

	@Test
	public void noOperand() {
		String input = Input.getFile("+");
		assertThat("no operator in a line", main(input), printsSomethingStartingWith(BAD_SYNTAX_MESSAGE));
		input = Input.getFile("+ 4 3", "* 2 5", "+ ");
		assertThat("no operator in a line", main(input), printsSomethingStartingWith(BAD_SYNTAX_MESSAGE));
		input = Input.getFile("+ 4 3", "* 2 5", "*");
		assertThat("no operator in a line", main(input), printsSomethingStartingWith(BAD_SYNTAX_MESSAGE));

	}

	@Test
	public void whitespaceHandling() {
		String input = Input.getFile("              ");
		assertThat(main(input), prints("1"));
		input = Input.getFile("    +       1  ", "              * 2 ", "*   2      ", "* 2   2 2 2    2",
				"    +    4 40", "*     3");
		assertThat("whitespace is allowed anywhere", main(input), prints("900"));
	}

	@Test
	public void letter() {
		String input = Input.getFile("+ 5 l");
		assertThat("only operators and numbers are allowed", main(input),
				printsSomethingStartingWith(BAD_SYNTAX_MESSAGE));
		input = Input.getFile("+ 4 3", "* 2 5", "3 c 5");
		assertThat("only operators and numbers are allowed", main(input),
				printsSomethingStartingWith(BAD_SYNTAX_MESSAGE));
		input = Input.getFile("- 5");
		assertThat("only operators and numbers are allowed", main(input),
				printsSomethingStartingWith(BAD_SYNTAX_MESSAGE));
		input = Input.getFile("+ 4 3", "* 2 5", "- 3 5");
		assertThat("only operators and numbers are allowed", main(input),
				printsSomethingStartingWith(BAD_SYNTAX_MESSAGE));
		input = Input.getFile("+ 5 l");
		assertThat("only operators and numbers are allowed", main(input),
				printsSomethingStartingWith(BAD_SYNTAX_MESSAGE));
		input = Input.getFile("+ + 3");
		assertThat("only operators and numbers are allowed", main(input),
				printsSomethingStartingWith(BAD_SYNTAX_MESSAGE));

		input = Input.getFile("+ 5 3", "* 2 5", "* * 3 5");
		assertThat("only operators and numbers are allowed", main(input),
				printsSomethingStartingWith(BAD_SYNTAX_MESSAGE));
	}

}
