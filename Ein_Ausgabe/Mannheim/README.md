Für den Judge wichtige Ordner und Dateien:
------------------------------------------
    data/sample/
    data/secret/
    domjudge-problem.ini
    problem.pdf

Der Rest wird ignoriert

Packen
------
Der Befehl `./gen_zip.sh -f name` erstellt ein zipfile das genau die benötigten Daten enthält.

Testfälle
---------
Das `data/generator.py` script produziert zufällige Testdaten.
