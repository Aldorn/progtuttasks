/** Class containing one method
* @author Joshua Gleitze
*/
public class Zitat {
	/** Puts input texts into other text.
	* @param args two texts as only arguments
	*/
	public static void main(String[] args) {
		System.out.println("Hallo " + args[0] + ", du sagst '" + args[1] + "'!");
		System.out.print("Das war gar nicht so schwer!");
	}
}
