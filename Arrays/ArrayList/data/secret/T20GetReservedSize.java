import test.TestObject;
import test.TestedClass;
import test.TestedProgram;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static test.matchers.UsedClassesMatcher.onlyUses;

import org.junit.AfterClass;
import org.junit.Test;

public class T20GetReservedSize {
	private final TestedClass TutorialArrayList = TestedProgram.getClass("edu.kit.informatik.util.TutorialArrayList");

	@Test
	public void getReservedSizeFromParameterless() {
		TestObject list = TutorialArrayList.nev();
		assertThat(list.run(int.class, "getReservedSize"), is(10));
	}
	
	@Test
	public void getReservedSizeWithInitialSize() {
		TestObject list = TutorialArrayList.nev(5);
		assertThat(list.run(int.class, "getReservedSize"), is(5));

		list = TutorialArrayList.nev(0);
		assertThat(list.run(int.class, "getReservedSize"), is(0));
	}
	
	@AfterClass
	public static void checkUsedPackages() {
		assertThat(TestedProgram.code, onlyUses("java.lang"));
	}
}
