package edu.kit.informatik;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.hamcrest.Matcher;
import static org.hamcrest.Matchers.*;

/**
 * A modified Terminal class which returns predefined answers on a call of
 * readLine and checks the outputs made with printLine to match certain
 * criteria.
 * 
 * The idea is to first specify an input and a matcher that checks the output
 * which is printed by the tested program upon that input. When the tested
 * program calls readLine the predefined input will be returned. Until the next
 * call of readLine all outputs will be stored. When readLine is called the
 * printed lines are checked by the matcher.
 * 
 * After the end of the tested program you have to call flush() to check the
 * latest output lines by hand.
 * 
 * @author moritz
 *
 */
public class Terminal {
    /**
     * The lines that will be issued by readLine.
     */
    private static LinkedList<String> inputLines = new LinkedList<String>();
    /**
     * Stores all lines printed by the tested program.
     */
    private static List<String> outputBuffer = new ArrayList<String>();
    /**
     * Stores matchers which verify the printed lines match specific conditions.
     */
    private static LinkedList<Matcher<? super List<String>>> matchers = new LinkedList<Matcher<? super List<String>>>();
    /**
     * Indicates the beginning of an interaction with the tested program.
     */
    private static boolean firstReadLine = true;

    /**
     * Is called by the program to test.
     * 
     * Checks all previously printed lines and return a next line to be read.
     * 
     * @return
     */
    public static String readLine() {
        // if we read the first line, we can't check any previous inputs.
        if (firstReadLine) {
            firstReadLine = false;
        } else {
            // check all previous output
            flush();
        }
        // the program to test should not try to read more lines than specified.
        // Make sure, you always quit it.
        if (inputLines.isEmpty()) {
            fail("There is no input specified that could be read.");
        }
        // return the line to read.
        return inputLines.removeFirst();
    }

    /**
     * Is called by the program to test.
     * 
     * A printed line will be buffered. All printed lines will be checked upon
     * the next call of readLine.
     * 
     * @param line
     *            The line to print.
     */
    public static void printLine(String line) {
        outputBuffer.add(line);
    }

    /**
     * Adds an input (a line that can be read by the tested program) and a
     * matcher to check all lines that will be printed until the next cal of
     * readLine.
     * 
     * @param input
     *            The input (e.g. a command).
     * @param matcher
     *            A matcher which checks a list (or supertype) of strings.
     */
    public static void addMultipleLineOutputThatMatches(String input, Matcher<? super List<String>> matcher) {
        inputLines.addLast(input);
        matchers.addLast(matcher);
    }

    /**
     * Convenience method. Specify several lines that should be exactly the
     * output of the program.
     * 
     * @param input
     * @param expectedOutput
     */
    public static void addMultipleLinesOutputThatIsExactly(String input, List<String> expectedOutput) {
        addMultipleLineOutputThatMatches(input, contains(expectedOutput.toArray()));
    }

    /**
     * Convenience method. Specify a matcher that checks a single line of
     * output. The tested program is expected to print out only a single line.
     * 
     * @param input
     *           
     * @param matcher
     */
    public static void addSingleLineOutputThatMatches(String input, Matcher<String> matcher) {
        addMultipleLineOutputThatMatches(input, allOf(iterableWithSize(1), hasItem(matcher)));
    }

    /**
     * Convenience method. Specify a single line of output that should be printed upon a call of input.
     * @param input
     * @param expectedOutput
     */
    public static void addSingleLineOutputThatIsExactly(String input, String expectedOutput) {
        addSingleLineOutputThatMatches(input, is(expectedOutput));
    }

    /**
     * Convenience method. Specify an input which expects no output.
     * @param input
     */
    public static void addNoOutput(String input) {
        addMultipleLineOutputThatMatches(input, empty());
    }

    /**
     * Checks the recently printed lines. Is done implicitly with every call of
     * readLine(). Use this method after the program was executed to check the
     * latest output lines.
     */
    public static void flush() {
        if (!matchers.isEmpty()) {
            // checks the recently printed lines
            assertThat(outputBuffer, matchers.removeFirst());
            outputBuffer.clear();
        }

    }

    /**
     * Use this method before you start a new interaction with the program to
     * test. It ensures that there won't be any output lines checked on the
     * first call of readLine.
     */
    public static void reset() {
        inputLines.clear();
        matchers.clear();
        outputBuffer.clear();
        firstReadLine = true;
    }

}
