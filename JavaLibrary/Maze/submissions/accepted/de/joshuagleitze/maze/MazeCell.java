package de.joshuagleitze.maze;

public class MazeCell {
	private final boolean[] walls = { false, false };
	final Coordinate position;
	boolean isVisited = false;
	final Maze maze;

	public MazeCell(Coordinate position, Maze maze) {
		this.position = position;
		this.maze = maze;
	}

	public MazeCell getNeigbour(Direction direction) {
		return this.maze.getCellAt(direction.move(this.position));
	}

	public boolean hasWallTo(Direction direction) {
		if (!direction.isForward()) {
			return this.getNeigbour(direction).hasWallTo(direction.getOpposite());
		}
		return this.walls[toWallIndex(direction)];
	}

	public void setHasWallTo(Direction direction) {
		if (!direction.isForward()) {
			this.getNeigbour(direction).setHasWallTo(direction.getOpposite());
		}
		this.walls[toWallIndex(direction)] = true;
	}

	private static int toWallIndex(Direction direction) {
		return direction.ordinal() % 2;
	}

	public void setVisited() {
		this.isVisited = true;
	}

	public void setUnvisited() {
		this.isVisited = false;
	}

	public boolean isVisited() {
		return this.isVisited;
	}

	public Coordinate getPosition() {
		return this.position;
	}
}
