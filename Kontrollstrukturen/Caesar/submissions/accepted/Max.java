class Caesar {
    private int key;
    private static final int ROT = 'Z' - 'A' + 1;
    private static final int ABC_MASK = 0x40;

    Caesar(int key) {
        this.key = key - 1;
    }
    
    private char rotateChar(char c, int k) {
        //return (c < 0x80 && ((c & ABC_MASK) != 0) && ((c | 0x60) != 0x60) && ((c & ~0x60) < 27)) ? (char)(((c & 0x1F) + k - 1) % ROT + 1 | c & ~0x1F) : c;
        //return (c >> 6) == 1 && (c & 0x1F) != 0 && ((c & 0x1F) < 27) ? (char)(((c & 0x1F) + k) % ROT + 1 | c & ~0x1F) : c;
        return (c >> 6) == 1 && ((char)((c & 0x1F) - 1)) < 26 ? (char)(((c & 0x1F) + k) % ROT + 1 | c & ~0x1F) : c;
    }
    
    public String encrypt(String s) {
        StringBuilder sb = new StringBuilder(s.length());
        for (int i = 0; i < s.length(); ++i) {
            sb.append(rotateChar(s.charAt(i), key));
        }
        return sb.toString();
    }
    
    public static void main(String... args) {
        Caesar c = new Caesar(args[1].toLowerCase().charAt(0) - 'a');
        System.out.println(c.encrypt(args[0]));
    }
}
